package captcha;

public class CaptchaTimer extends Thread {
    private final String sessionId;
    private boolean captchaIsTyped;
    private final CaptchaCreator captchaCreator;
    public boolean captchaIsCreated = false;

    public CaptchaTimer(String sessionId) {
        this.sessionId = sessionId;
        this.captchaIsTyped = false;
        captchaCreator = new CaptchaCreator();
    }

    public int getCaptchaValue() {
        return captchaCreator.getCaptchaValue();
    }

    public void captchaIsTyped() {
        captchaIsTyped = true;
    }

    @Override
    public void run() {
        int timer = 5;
        int timerSecond = timer * 60 * 1000;
        captchaCreator.createCaptcha(sessionId);
        captchaIsCreated = true;
        int currentTime = 0;
        while (currentTime < timerSecond && !captchaIsTyped) {
            try {
                Thread.sleep(1);
                currentTime += 1;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        captchaCreator.deleteImg();
    }
}