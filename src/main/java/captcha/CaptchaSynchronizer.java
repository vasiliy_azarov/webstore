package captcha;

import java.util.HashMap;
import java.util.Map;

public class CaptchaSynchronizer {
    private static CaptchaSynchronizer instance;

    private final Map<String, CaptchaTimer> captchaMap;

    private CaptchaSynchronizer() {
        captchaMap = new HashMap<String, CaptchaTimer>();
    }

    public Map<String, CaptchaTimer> getCaptchaMap() {
        return captchaMap;
    }

    public static CaptchaSynchronizer getInstance() {
        if (instance == null) {
            instance = new CaptchaSynchronizer();
        }
        return instance;
    }
}