package captcha;

import java.util.ResourceBundle;

public class CaptchaModeSelector {

    public boolean isCookie() {
        ResourceBundle rb = ResourceBundle.getBundle("captchaSelector");
        String mode = (String) rb.getObject(rb.getKeys().nextElement());
        return !mode.equals("session");
    }
}