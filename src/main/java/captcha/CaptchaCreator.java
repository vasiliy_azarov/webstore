package captcha;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

public class CaptchaCreator {
    Random randomNumber = new Random();
    int captchaValue = randomNumber.nextInt(8999) + 1000;
    private final String str = String.valueOf(captchaValue);
    private String pathOfImg;

    public void deleteImg() {
        if (pathOfImg != null) {
            File file = new File(pathOfImg);
            file.delete();
            pathOfImg = null;
        }
    }

    public int getCaptchaValue() {
        return captchaValue;
    }

    private static BufferedImage readImg(String name) {
        File file = new File("src/main/webapp/resources/captcha/" + name + ".jpg");
        BufferedImage bufferedImage = null;
        try {
            bufferedImage = ImageIO.read(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bufferedImage;
    }

    public void createCaptcha(String id) {
        int[] num = new int[str.length()];
        BufferedImage im = new BufferedImage(100 * str.length(), 150, BufferedImage.TYPE_INT_RGB);
        for (int i = 0, j = 0; i < num.length; i++, j += 100) {
            num[i] = Integer.parseInt(str.substring(i, i + 1));
            im.getGraphics().drawImage(readImg(String.valueOf(num[i])), j, 0, null);
        }
        try {
            String TempPath = "src/main/webapp/resources/captcha/result" + id + ".jpg";
            File file = new File(TempPath);
            setPathOfImg(TempPath);
            ImageIO.write(im, "jpg", file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setPathOfImg(String pathOfImg) {
        this.pathOfImg = pathOfImg;
    }
}