package service;

import entity.Item;
import repository.ItemRepository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Sorter {
    private final String selectedName;
    private final String selectedOrder;
    private final String selectedMinPrice;
    private final String selectedMaxPrice;
    private final String selectedManufacturer;
    private final ItemRepository itemRepository;

    public Sorter(String selectedName, String selectedOrder, String selectedMinPrice, String selectedMaxPrice, String selectedManufacturer, ItemRepository itemRepository) {
        this.selectedName = selectedName;
        this.selectedOrder = selectedOrder;
        this.selectedMinPrice = selectedMinPrice;
        this.selectedMaxPrice = selectedMaxPrice;
        this.selectedManufacturer = selectedManufacturer;
        this.itemRepository = itemRepository;
    }


    public List<Item> getSortedItemList(int skippedNumbers, int gatedNumbers) throws SQLException {
        List<Item> list = new ArrayList<>();
        if (selectedName.equals("") && selectedMinPrice.equals("") && selectedMaxPrice.equals("") && selectedManufacturer.equals("")) {
            list = itemRepository.sortDefault(skippedNumbers, gatedNumbers, defineOrder(selectedOrder));
        }
        if (!selectedName.equals("") && selectedMinPrice.equals("") && selectedMaxPrice.equals("") && selectedManufacturer.equals("")) {
            list = itemRepository.createSortedListSortedByName(selectedName, skippedNumbers, gatedNumbers, defineOrder(selectedOrder));
        }
        if (!selectedName.equals("") && !selectedMinPrice.equals("") && !selectedMaxPrice.equals("") && selectedManufacturer.equals("")) {
            list = itemRepository.createSortedListSortedByNameAndPrice(selectedName, Integer.parseInt(selectedMinPrice), Integer.parseInt(selectedMaxPrice), skippedNumbers, gatedNumbers, defineOrder(selectedOrder));
        }
        if (!selectedName.equals("") && selectedMinPrice.equals("") && selectedMaxPrice.equals("") && !selectedManufacturer.equals("")) {
            list = itemRepository.createSortedListSortedByNameAndManufacturer(selectedName, selectedManufacturer, skippedNumbers, gatedNumbers, defineOrder(selectedOrder));
        }
        if (!selectedName.equals("") && !selectedMinPrice.equals("") && !selectedMaxPrice.equals("") && !selectedManufacturer.equals("")) {
            list = itemRepository.createSortedListSortedByNameByPriceAndManufacturer(selectedName, selectedManufacturer, Integer.parseInt(selectedMinPrice), Integer.parseInt(selectedMaxPrice), skippedNumbers, gatedNumbers, defineOrder(selectedOrder));
        }
        if (selectedName.equals("") && !selectedMinPrice.equals("") && !selectedMaxPrice.equals("") && selectedManufacturer.equals("")) {
            list = itemRepository.createSortedListSortedByPrice(Integer.parseInt(selectedMinPrice), Integer.parseInt(selectedMaxPrice), skippedNumbers, gatedNumbers, defineOrder(selectedOrder));
        }
        if (selectedName.equals("") && !selectedMinPrice.equals("") && !selectedMaxPrice.equals("") && !selectedManufacturer.equals("")) {
            list = itemRepository.createSortedListSortedByPriceAndManufacturer(selectedManufacturer, Integer.parseInt(selectedMinPrice), Integer.parseInt(selectedMaxPrice), skippedNumbers, gatedNumbers, defineOrder(selectedOrder));
        }
        if (selectedName.equals("") && selectedMinPrice.equals("") && selectedMaxPrice.equals("") && !selectedManufacturer.equals("")) {
            list = itemRepository.createSortedListSortByManufacturer(selectedManufacturer, skippedNumbers, gatedNumbers, defineOrder(selectedOrder));
        }
        return list;
    }

    public String defineOrder(String order) {
        String definedOrder = "";
        if (order.equals("name(A-Z)")) {
            definedOrder = "name";
        }
        if (order.equals("name(Z-A)")) {
            definedOrder = "name DESC";
        }
        if (order.equals("price(waning)")) {
            definedOrder = "price DESC";
        }
        if (order.equals("price(increase)")) {
            definedOrder = "price";
        }
        if (order.equals("manufacturer(A-Z)")) {
            definedOrder = "manufacturer";
        }
        if (order.equals("manufacturer(Z-A)")) {
            definedOrder = "manufacturer DESC";
        }
        if (order.equals("quantity(waning)")) {
            definedOrder = "quantity DESC";
        }
        if (order.equals("quantity(increase)")) {
            definedOrder = "quantity";
        }
        return definedOrder;
    }
}