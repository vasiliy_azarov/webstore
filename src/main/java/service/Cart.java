package service;

import entity.Item;
import repository.ItemRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Cart {
   private Map<Item, Integer> cartMap = new HashMap<>();
   private ItemRepository itemRepository;

    public Cart(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public boolean addItemToCart(long itemId) {
        if (itemRepository.getItemById(itemId) == null) {
            return false;
        }
        int itemNumber = 1;
       if(cartMap.containsKey(itemRepository.getItemById(itemId))){
           itemNumber= cartMap.get(itemRepository.getItemById(itemId))+1;
       }
            cartMap.put(itemRepository.getItemById(itemId), itemNumber);
        return true;
    }

    public boolean deleteItemFromCart(long itemId) {
        if (itemRepository.getItemById(itemId) == null || !cartMap.containsKey(itemRepository.getItemById(itemId))) {
            return false;
        }
        int numberOfItemInCart = cartMap.get(itemRepository.getItemById(itemId));
        if (numberOfItemInCart > 1) {
            cartMap.put(itemRepository.getItemById(itemId), numberOfItemInCart - 1);
        } else {
            cartMap.remove(itemRepository.getItemById(itemId));
        }
        return true;
    }

    public List<Item> getListOfAllProductsInCart() {
        List<Item> cartList = new ArrayList<>();
        for (Item item: cartMap.keySet() ) {
        cartList.add(item);
        }
        return cartList;
    }
    public void clearCart(){
        cartMap.clear();
    }

    public int getNumberOfAllProductsInCart(){
        int numberOfAllProductsInCart=0;
        for (Item item: cartMap.keySet() ) {
            numberOfAllProductsInCart += cartMap.get(item);
        }
        return numberOfAllProductsInCart;
    }
    public  int getFullPrice() {
        int fullPrice = 0;
        for (Item item : cartMap.keySet()) {
            fullPrice += item.getPrice() * cartMap.get(item);
        }
        return fullPrice;
    }
}