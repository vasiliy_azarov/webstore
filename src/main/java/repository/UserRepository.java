package repository;


import entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserRepository implements DaoUser {
    DBConnector dbConnector = new DBConnector();
    int userIdColumnIndex = 1;
    int userNameColumnIndex = 2;
    int userSurnameColumnIndex = 3;
    int userEmailColumnIndex = 4;
    int userPasswordColumnIndex = 5;
    int userAvatarPathColumnIndex = 6;
    int userNewslettersColumnIndex = 7;


    public List<User> getAll() {
        List<User> list = new ArrayList<User>();
        try {
            Connection connection = dbConnector.getConnection();
            connection.setAutoCommit(false);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM webstoredb.user;");
            while (resultSet.next()) {
                list.add(new User(resultSet.getLong(userIdColumnIndex), resultSet.getString(userNameColumnIndex), resultSet.getString(userSurnameColumnIndex), resultSet.getString(userEmailColumnIndex), resultSet.getString(userPasswordColumnIndex), resultSet.getString(userAvatarPathColumnIndex), resultSet.getBoolean(userNewslettersColumnIndex)));
            }
            connection.commit();
            resultSet.close();
            statement.close();
            connection.close();
        } catch (Exception throwable) {
            throwable.printStackTrace();
        }
        return list;
    }

    public boolean addUser(User user) {
        if (user.getId() == 0) {
            try {
                Connection connection = dbConnector.getConnection();
                connection.setAutoCommit(false);
                PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO user (name, surname, email, password, avatarPath, newsletters) VALUES (?, ?,?,?,?,?)");
                preparedStatement.setString(1, user.getName());
                preparedStatement.setString(2, user.getSurname());
                preparedStatement.setString(3, user.getEmail());
                preparedStatement.setString(4, user.getPassword());
                preparedStatement.setString(5, user.getAvatarPath());
                preparedStatement.setBoolean(6, user.getNewsletters());
                preparedStatement.executeUpdate();
                connection.commit();
                preparedStatement.close();
                return true;
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return false;
    }

    public boolean deleteUserById(long id) {
        if (getUserById(id) == null) {
            return false;
        }
        try {
            Connection connection = dbConnector.getConnection();
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM webstoredb.user WHERE id=?;");
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
            connection.commit();
            preparedStatement.close();
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return true;
    }

    public User getUserById(long id) {
        User user = null;
        try {
            Connection connection = dbConnector.getConnection();
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM webstoredb.user WHERE id=?;");
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getResultSet();
            user = new User(resultSet.getLong(userIdColumnIndex), resultSet.getString(userNameColumnIndex),
                    resultSet.getString(userSurnameColumnIndex), resultSet.getString(userEmailColumnIndex),
                    resultSet.getString(userPasswordColumnIndex), resultSet.getString(userAvatarPathColumnIndex),
                    resultSet.getBoolean(userNewslettersColumnIndex));
            connection.commit();
            resultSet.close();
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return user;
    }
}