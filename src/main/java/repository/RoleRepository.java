package repository;

import entity.Role;

import java.sql.*;

public class RoleRepository {
    DBConnector dbConnector = new DBConnector();
    public Role getUserRoleByUserId(int userId) {
        String query = "SELECT * FROM webstoredb.role WHERE user_id="+userId+";";
        Role role = null;
        try {
            Connection connection = dbConnector.getConnection();
            connection.setAutoCommit(false);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            resultSet.next();
                role =  new Role(resultSet.getInt(1), resultSet.getString(2),
                        resultSet.getInt(3));
            connection.commit();
            resultSet.close();
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return role;
    }
}