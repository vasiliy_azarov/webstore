package repository;

import entity.Item;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ItemRepository implements DaoItem {
    DBConnector dbConnector = new DBConnector();
    int itemIdColumnIndex = 1;
    int itemNameColumnIndex = 2;
    int itemPriceColumnIndex = 3;
    int itemQuantityColumnIndex = 4;
    int itemManufacturerColumnIndex = 5;
    int itemDescriptionColumnIndex = 6;

    public List<Item> getAll() {
        List<Item> itemList = new ArrayList<>();
        int limit = 0;
        int number = 3;
        String order = "name";
        try {
            itemList = sortDefault(limit, number, order);
            return itemList;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return itemList;
    }

    public boolean addItem(Item item) {
        if (item.getId() == 0) {
            try {
                Connection connection = dbConnector.getConnection();
                connection.setAutoCommit(false);
                PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO item (name, price, quantity, manufacturer, description) VALUES (?, ?,?,?,?)");
                preparedStatement.setString(1, item.getName());
                preparedStatement.setInt(2, item.getPrice());
                preparedStatement.setInt(3, item.getQuantity());
                preparedStatement.setString(4, item.getManufacturer());
                preparedStatement.setString(5, item.getDescription());
                preparedStatement.executeUpdate();
                connection.commit();
                preparedStatement.close();
                connection.close();
                return true;
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return false;
    }

    public boolean deleteItemById(long id) {
        if (getItemById(id) == null) {
            return false;
        }
        try {
            Connection connection = dbConnector.getConnection();
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM webstoredb.item WHERE id=?;");
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
            connection.commit();
            preparedStatement.close();
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return true;
    }

    public Item getItemById(long id) {
        Item item = null;
        String query = "SELECT*FROM item WHERE id=" + id + " ;";
        try {
            List<Item> list = createItemList(query);
            item = list.get(0);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return item;
    }

    public List<Item> createItemList(String query) throws SQLException {
        Connection connection = dbConnector.getConnection();
        connection.setAutoCommit(false);
        List<Item> list = new ArrayList<>();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            list.add(new Item(resultSet.getLong(itemIdColumnIndex), resultSet.getString(itemNameColumnIndex),
                    resultSet.getInt(itemPriceColumnIndex), resultSet.getInt(itemQuantityColumnIndex),
                    resultSet.getString(itemManufacturerColumnIndex), resultSet.getString(itemDescriptionColumnIndex)));
        }
        connection.commit();
        statement.close();
        resultSet.close();
        connection.close();
        return list;
    }

    public List<Item> sortDefault(int limit, int number, String order) throws SQLException {
        String query = "SELECT * FROM webstoredb.item ORDER BY " + order + " LIMIT " + limit + ", " + number;
        return createItemList(query);
    }

    public List<Item> createSortedListSortedByName(String name, int limit, int number, String order) throws SQLException {
        String query = "SELECT * FROM webstoredb.item WHERE name= '" + name + "' ORDER BY " + order + " LIMIT " + limit + ", " + number;
        return createItemList(query);
    }

    public List<Item> createSortedListSortedByNameAndPrice(String name, int selectedMinPrice, int selectedMaxPrice, int limit, int number, String order) throws SQLException {
        String query = "SELECT * FROM webstoredb.item WHERE name= '" + name + "' AND price BETWEEN " + selectedMinPrice + " AND " + selectedMaxPrice + " ORDER BY " + order + " LIMIT " + limit + ", " + number;
        return createItemList(query);
    }

    public List<Item> createSortedListSortedByNameAndManufacturer(String name, String manufacturer, int limit1, int number, String order) throws SQLException {
        String query = "SELECT * FROM webstoredb.item WHERE name= '" + name + "' AND manufacturer= '" + manufacturer + "' ORDER BY " + order + " LIMIT " + limit1 + ", " + number;
        return createItemList(query);
    }

    public List<Item> createSortedListSortedByNameByPriceAndManufacturer(String name, String manufacturer, int selectedMinPrice, int selectedMaxPrice, int limit, int number, String order) throws SQLException {
        String query = "SELECT * FROM webstoredb.item WHERE name= '" + name + "' AND manufacturer= '" + manufacturer + "' AND price BETWEEN " + selectedMinPrice + " AND " + selectedMaxPrice + " ORDER BY " + order + " LIMIT " + limit + ", " + number;
        return createItemList(query);
    }

    public List<Item> createSortedListSortedByPrice(int selectedMinPrice, int selectedMaxPrice, int limit, int number, String order) throws SQLException {
        String query = "SELECT * FROM webstoredb.item WHERE price BETWEEN " + selectedMinPrice + " AND " + selectedMaxPrice + " ORDER BY " + order + " LIMIT " + limit + ", " + number;
        return createItemList(query);
    }

    public List<Item> createSortedListSortedByPriceAndManufacturer(String manufacturer, int selectedMinPrice, int selectedMaxPrice, int limit, int number, String order) throws SQLException {
        String query = "SELECT * FROM webstoredb.item WHERE manufacturer= '" + manufacturer + "' AND price BETWEEN " + selectedMinPrice + " AND " + selectedMaxPrice + " ORDER BY " + order + " LIMIT " + limit + ", " + number;
        return createItemList(query);
    }

    public List<Item> createSortedListSortByManufacturer(String manufacturer, int limit, int number, String order) throws SQLException {
        String query = "SELECT * FROM webstoredb.item WHERE manufacturer= '" + manufacturer + "' ORDER BY " + order + " LIMIT " + limit + ", " + number;
        return createItemList(query);
    }
}