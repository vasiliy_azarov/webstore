package repository;

import entity.User;

import java.util.List;

public interface DaoUser {

    List<User> getAll();

    boolean addUser(User user);

    boolean deleteUserById(long id);

    User getUserById(long id);
}