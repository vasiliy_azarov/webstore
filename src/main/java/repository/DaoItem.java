package repository;

import entity.Item;

import java.util.List;

public interface DaoItem {

    List<Item> getAll();

    boolean addItem(Item item);

    boolean deleteItemById(long id);

    Item getItemById(long id);
}