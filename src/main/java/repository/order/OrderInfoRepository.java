package repository.order;

import entity.order.Order;
import entity.order.OrderInfo;
import repository.DBConnector;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderInfoRepository {
    DBConnector dbConnector = new DBConnector();
    int orderInfoIdColumnIndex = 1;
    int orderInfoStatusColumnIndex = 2;
    int orderInfoStatusDescriptionColumnIndex = 3;
    int orderInfoDateColumnIndex = 4;
    int orderInfoUserIdColumnIndex = 5;

    public List<OrderInfo> getAll() {
        List<OrderInfo> orderInfoList = new ArrayList<>();
        String query = "SELECT * FROM webstoredb.order_info;";
        try {
            orderInfoList = createOrderInfoList(query);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return orderInfoList;
    }

    public boolean addOrderInfo(OrderInfo orderInfo) {
        if (orderInfo.getId() == 0) {
            try {
                Connection connection = dbConnector.getConnection();
                connection.setAutoCommit(false);
                PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO order_info (order_status, status_description, date, user_id, order_list) VALUES (?,?,?,?,?)");
                preparedStatement.setString(1, orderInfo.getOrderStatus());
                preparedStatement.setString(2, orderInfo.getStatusDescription());
                preparedStatement.setString(3, orderInfo.getDate());
                preparedStatement.setLong(4, orderInfo.getUserId());
                preparedStatement.setString(5, parseOrderListToString(orderInfo.getOrderList()));
                preparedStatement.executeUpdate();
                connection.commit();
                preparedStatement.close();
                return true;
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return false;
    }

    public boolean deleteOrderInfoById(long id) {
        if (getOrderInfoById(id) == null) {
            return false;
        }
        try {
            Connection connection = dbConnector.getConnection();
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM webstoredb.order_info WHERE id=?;");
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
            connection.commit();
            preparedStatement.close();
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return true;
    }

    public OrderInfo getOrderInfoById(long id) {
        OrderInfo orderInfo = null;
        String query = "SELECT*FROM order_info WHERE id=" + id + " ;";
        try {
            List<OrderInfo> list = createOrderInfoList(query);
            orderInfo = list.get(0);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return orderInfo;
    }

    public OrderInfo getOrderInfoByUserId(long id) {
        OrderInfo orderInfo = null;
        String query = "SELECT*FROM order_info WHERE user_id=" + id + " ;";
        try {
            List<OrderInfo> list = createOrderInfoList(query);
            orderInfo = list.get(0);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return orderInfo;
    }

    public List<OrderInfo> createOrderInfoList(String query) throws SQLException {
        Connection connection = dbConnector.getConnection();
        List<OrderInfo> list = new ArrayList<>();
        OrderRepository orderRepository = new OrderRepository();
        connection.setAutoCommit(false);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            list.add(new OrderInfo(resultSet.getLong(orderInfoIdColumnIndex), resultSet.getString(orderInfoStatusColumnIndex),
                    resultSet.getString(orderInfoStatusDescriptionColumnIndex), resultSet.getString(orderInfoDateColumnIndex),
                    resultSet.getLong(orderInfoUserIdColumnIndex), parseOrderStringToList(resultSet.getString(orderInfoIdColumnIndex))));
        }
        connection.commit();
        statement.close();
        resultSet.close();
        connection.close();
        return list;
    }

    public List<Order> parseOrderStringToList(String orderId) {
        List<Order> orderList = new ArrayList<>();
        OrderRepository orderRepository = new OrderRepository();
        for (int i = 0; i < orderId.length(); i++) {
            String s = String.valueOf(orderId.charAt(i));
            if (!s.equals(" ") && !s.equals(",")) {
                orderList.add(orderRepository.getOrderById(Integer.parseInt(s)));
            }
        }
        return orderList;
    }

    public String parseOrderListToString(List<Order> orderList) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Order order : orderList) {
            stringBuilder.append(order.getId() + ", ");
        }
        return String.valueOf(stringBuilder);
    }
}