package repository.order;

import entity.order.Order;
import repository.DBConnector;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderRepository {
    DBConnector dbConnector = new DBConnector();
    int orderIdColumnIndex = 1;
    int orderDescriptionColumnIndex = 2;
    int orderNumberOfProductsColumnIndex = 3;
    int orderPriceColumnIndex = 4;

    public List<Order> getAll() {
        List<Order> orderList = new ArrayList<>();
        String query = "SELECT*FROM webstoredb.product_order";
        try {
            orderList = createOrderList(query);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return orderList;
    }

    public boolean addOrder(Order order) {
        if (order.getId() == 0) {
            try {
                Connection connection = dbConnector.getConnection();
                connection.setAutoCommit(false);
                PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO product_order (order_description, number_of_products, price) VALUES (?,?,?)");
                preparedStatement.setString(1, order.getOrderDescription());
                preparedStatement.setInt(2, order.getNumberOfProducts());
                preparedStatement.setInt(3, order.getPrice());
                preparedStatement.executeUpdate();
                connection.commit();
                preparedStatement.close();
                return true;
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return false;
    }

    public Order getOrderById(long id) {
        Order order = null;
        String query = "SELECT*FROM webstoredb.product_order WHERE id=" + id + " ;";
        try {
            List<Order> list = createOrderList(query);
            order = list.get(0);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return order;
    }


    public boolean deleteOrderById(long id) {
        if (getOrderById(id) == null) {
            return false;
        }
        try {
            Connection connection = dbConnector.getConnection();
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM webstoredb.product_order WHERE id=?;");
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
            connection.commit();
            preparedStatement.close();
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return true;
    }


    public List<Order> createOrderList(String query) throws SQLException {
        Connection connection = dbConnector.getConnection();
        connection.setAutoCommit(false);
        List<Order> list = new ArrayList<>();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            list.add(new Order(resultSet.getLong(orderIdColumnIndex), resultSet.getString(orderDescriptionColumnIndex),
                    resultSet.getInt(orderNumberOfProductsColumnIndex), resultSet.getInt(orderPriceColumnIndex)));
        }
        connection.commit();
        statement.close();
        resultSet.close();
        connection.close();
        return list;
    }

}