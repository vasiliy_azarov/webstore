package web.filters;

import entity.Role;
import entity.User;
import repository.RoleRepository;
import utils.Path;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public class AccessFilter implements Filter{
    Map<String, String> roleUriMap;

    @Override
    public void init(FilterConfig filterConfig) {
        roleUriMap = (Map<String, String>) filterConfig.getServletContext().getAttribute("roleUriMap");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req= (HttpServletRequest) servletRequest;
        HttpServletResponse resp= (HttpServletResponse) servletResponse;
        String uri = req.getRequestURI();

        if (roleUriMap.containsKey(uri)) {
            User user = (User) req.getSession().getAttribute("userLogin");
            RoleRepository roleRepository = (RoleRepository) req.getServletContext().getAttribute("roleRepository");
            if (user != null) {
                Role role = roleRepository.getUserRoleByUserId((int) user.getId());
                if (roleUriMap.get(uri).equals(role.getRole())) {
                    filterChain.doFilter(req, resp);
                } else {
                    req.getRequestDispatcher(Path.ERROR_PAGE).forward(req, resp);
                }
            } else {
                req.getRequestDispatcher(Path.LOGIN_PAGE).forward(req, resp);
            }
        } else filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }
}
