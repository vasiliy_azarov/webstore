package web.filters;

import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.ServletException;
import javax.servlet.FilterChain;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

public class LanguageFilter implements Filter {
    Cookie cookieLang = null;
    Object locale;
    boolean cookieMode = false;
    boolean sessionMode = false;
    int cookieLifeInSeconds;
    HttpServletResponse resp;

    public void init(FilterConfig config) {
        if (config.getInitParameter("cookie").equals("true")) {
            cookieMode = true;
            int cookieLifeInDays = Integer.parseInt(config.getInitParameter("cookieLifeInDays"));
            cookieLifeInSeconds = cookieLifeInDays * 24 * 60 * 60;
        }
        if (config.getInitParameter("session").equals("true"))
            sessionMode = true;
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        List locales = new ArrayList<>();
        locales.add(Locale.getDefault());
        locales.add(Locale.GERMANY);
        locales.add(Locale.US);
        Enumeration<Locale> loc = req.getLocales();
        if (sessionMode) {
            if (req.getSession().getAttribute("lang") == null) {
                while (loc.hasMoreElements()) {
                    Locale currentLocale = loc.nextElement();
                    if (locales.contains(currentLocale)) {
                        req.setAttribute("lang", currentLocale);
                        break;
                    } else {
                        req.setAttribute("lang", Locale.getDefault());
                    }
                }
            } else {
                req.setAttribute("lang", req.getSession().getAttribute("lang"));
            }
        }
        if (cookieMode) {
            Cookie[] cookies = req.getCookies();
            String cookieName = "lang";
            if (cookies != null) {
                for (Cookie c : cookies) {
                    if (cookieName.equals(c.getName())) {
                        cookieLang = c;
                        req.setAttribute("lang", cookieLang.getValue());
                        break;
                    } else {
                        if (req.getSession().getAttribute("lang") == null) {
                            while (loc.hasMoreElements()) {
                                Locale currentLocale = loc.nextElement();
                                if (locales.contains(currentLocale)) {
                                    req.setAttribute("lang", currentLocale);
                                    break;
                                }
                            }
                        }
                    }
                }
            }else {
                req.setAttribute("lang", Locale.getDefault());
            }locale = req.getAttribute("lang");
        }
        resp = res;
        req.getSession().setAttribute("lang", req.getAttribute("lang"));
        chain.doFilter(request, response);
    }

    public void destroy() {
        if (cookieMode) {
            cookieLang = new Cookie("lang", locale.toString());
            cookieLang.setMaxAge(cookieLifeInSeconds);
            resp.addCookie(cookieLang);
        }
    }
}