package web.tags;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class LanguageSwitcherTag extends SimpleTagSupport {

    public void doTag()
            throws IOException {
        JspWriter out = getJspContext().getOut();
        out.print("<form id=\"changeLanguage\" name=\"changeLanguage\" method=\"get\" action=\"MainServlet\">\n" +
                "    <input type=\"hidden\" name=\"command\" value=\"ChangeLanguage\" >\n" +
                "    <button type=\"submit\" value=\"EN\" name=\"changeLang\" >EN</button>\n" +
                "    <button type=\"submit\" value=\"GE\" name=\"changeLang\" >GE</button>\n" +
                "    <button type=\"submit\" value=\"RU\" name=\"changeLang\" >RU</button>\n" +
                "</form>");
    }
}
