package web.tags;

import entity.User;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class LoginTag extends SimpleTagSupport {

    public void doTag()
            throws IOException {
        JspWriter out = getJspContext().getOut();
        PageContext pageContext = (PageContext) getJspContext();
        Object session = pageContext.getSession().getAttribute("userLogin");
        User user = (User) session;
        if (session != null) {
            out.print("<div id=\"signin\">\n" +
                    "<input type=\"hidden\" name=\"command\" value=\"Logout\">\n" +
                    user.getName() +
                    "<img id=\"userAvatarImg\" name=\"userAvatarImg\" src=\"resources/userAvatarStorage/avatar" + user.getEmail() + ".jpg\" width=\"100\" height=\"100\" alt=\"avatar\"/>\n" +
                    "<button type=\"submit\" id=\"logout\" value=\"Logout\">Logout</button>\n" +
                    "</div>\n");
        } else {
            out.print("<div id=\"signin\">\n" +
                    "        <input type=\"hidden\" name=\"command\" value=\"Login\">\n" +
                    "        <input type=\"text\" id=\"user\" name=\"user\" placeholder=\"email\"/>\n" +
                    "        <input type=\"password\" id=\"pass\" name=\"pass\" placeholder=\"password\"/>\n" +
                    "        <button type=\"submit\" id=\"btn\" value=\"Login\">&#xf0da;</button>\n" +
                    "    </div>\n" +
                    "    <div id=\"message\"></div>\n");
        }
    }
}