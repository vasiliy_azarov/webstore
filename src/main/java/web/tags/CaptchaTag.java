package web.tags;

import utils.CaptchaModeSelector;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class CaptchaTag extends SimpleTagSupport {

    public void doTag()
            throws IOException {
        CaptchaModeSelector captchaModeSelector = new CaptchaModeSelector();
        JspWriter out = getJspContext().getOut();
        if (captchaModeSelector.isCookie()) {
            out.print("<input id=\"expectedCaptchaValue\" name=\"expectedCaptchaValue\", type=\"hidden\" value=\"" + getJspContext().findAttribute("expectedCaptchaValue") + "\">\n" +
                    "<img id=\"captchaImg\" name=\"captchaImg\" src=\"resources/captcha/result" + getJspContext().findAttribute("sessionId") + ".jpg\" width=\"320\" height=\"111\" alt=\"captcha->0000\"/>\n" +
                    "<input id=\"captcha\" name=\"captcha\" type=\"text\" placeholder=\"Enter captcha\">");
        } else {
            out.print("<img id=\"captchaImg\" name=\"captchaImg\" src=\"resources/captcha/result" + getJspContext().findAttribute("sessionId") + ".jpg\" width=\"320\" height=\"111\" alt=\"captcha->0000\"/>\n" +
                    "<input id=\"captcha\" name=\"captcha\" type=\"text\" placeholder=\"Enter captcha\">");

        }
    }
}