package web.command;

import java.util.Map;
import java.util.TreeMap;

public class CommandContainer {
    private static final Map<String, Command> commands = new TreeMap<String, Command>();

    static {
        commands.put("Login", new CommandLogin());
        commands.put("Registration", new CommandRegistration());
        commands.put("GoToRegistrationPage", new CommandGoToRegistrationPage());
        commands.put("GoToLoginPage", new CommandGoToLoginPage());
        commands.put("Logout", new CommandLogout());
        commands.put("Sort", new CommandSort());
        commands.put("BuyItem", new CommandBuyItem());
        commands.put("GoToCartPage", new CommandGoToCartPage());
        commands.put("ChangeNumberOfProduct", new CommandChangeNumberOfProduct());
        commands.put("GoToOrderPage", new CommandGoToOrderPage());
        commands.put("CreateOrder", new CommandCreateOrder());
        commands.put("ChangeLanguage", new CommandChangeLanguage());
    }

    public static Command get(String commandName) {
        if (commandName == null || !commands.containsKey(commandName)) {
            return commands.get("noCommand");
        }
        return commands.get(commandName);
    }
}