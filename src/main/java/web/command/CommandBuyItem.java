package web.command;

import repository.ItemRepository;
import service.Cart;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static utils.Path.CATALOG_PAGE;

public class CommandBuyItem extends Command {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        Cart cart;
        if (request.getSession().getAttribute("cart") == null) {
            ItemRepository itemRepository = (ItemRepository) request.getServletContext().getAttribute("itemRepository");
            cart = new Cart(itemRepository);
        } else {
            cart = (Cart) request.getSession().getAttribute("cart");
        }
        long itemId = Integer.valueOf(request.getParameter("buy"));
        request.setAttribute("nameSelect", request.getSession().getAttribute("nameSelect"));
        request.setAttribute("orderSortSelect", request.getSession().getAttribute("orderSortSelect"));
        request.setAttribute("startPriceSelect", request.getSession().getAttribute("startPriceSelect"));
        request.setAttribute("endPriceSelect", request.getSession().getAttribute("endPriceSelect"));
        request.setAttribute("manufacturerSelect", request.getSession().getAttribute("manufacturerSelect"));
        request.setAttribute("selectedOrder", request.getSession().getAttribute("selectedOrder"));
        request.setAttribute("numberOfItem", request.getSession().getAttribute("numberOfItem"));
        request.setAttribute("itemList", request.getSession().getAttribute("itemList"));
        cart.addItemToCart(itemId);
        request.getSession().setAttribute("cart", cart);
        request.setAttribute("numberOfProductsInCart", cart.getNumberOfAllProductsInCart());
        request.setAttribute("totalPrice", cart.getFullPrice());
        return CATALOG_PAGE;
    }
}