package web.command;

import utils.CaptchaModeSelector;
import captcha.CaptchaSynchronizer;
import captcha.CaptchaTimer;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static utils.Path.REGISTRATION_PAGE;

public class CommandGoToRegistrationPage extends Command {
    private static final String EXPECTED_CAPTCHA_VALUE_ATTRIBUTE = "expectedCaptchaValue";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        CaptchaSynchronizer instance = CaptchaSynchronizer.getInstance();
        Map<String, CaptchaTimer> captchaMap = instance.getCaptchaMap();
        if (captchaMap.get(request.getSession().getId()) == null) {
            CaptchaTimer captchaTimer = new CaptchaTimer(request.getSession().getId());
            captchaMap.put(request.getSession().getId(), captchaTimer);
            captchaTimer.start();
        }
        CaptchaModeSelector captchaModeSelector = new CaptchaModeSelector();
        if (captchaModeSelector.isCookie()) {
            Cookie cookie = new Cookie(EXPECTED_CAPTCHA_VALUE_ATTRIBUTE, String.valueOf(instance.getCaptchaMap().get(request.getSession().getId()).getCaptchaValue()));
            response.addCookie(cookie);
            request.setAttribute(EXPECTED_CAPTCHA_VALUE_ATTRIBUTE, cookie.getName());
        }
        request.setAttribute("sessionId", request.getSession().getId());
        request.setAttribute(EXPECTED_CAPTCHA_VALUE_ATTRIBUTE, request.getSession().getId());
        return REGISTRATION_PAGE;
    }
}