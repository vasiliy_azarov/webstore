package web.command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;

public abstract class Command implements Serializable {
    private static final long serialVersionUID = 8879403039606311780L;

    public abstract String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException;

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}