package web.command;

import entity.User;
import entity.order.Order;
import entity.order.OrderInfo;
import repository.order.OrderInfoRepository;
import repository.order.OrderRepository;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import static utils.Path.ORDER_PAGE;

public class CommandCreateOrder extends Command {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
        Order currentOrder = (Order) request.getSession().getAttribute("currentOrder");
        String selectedDelivery = request.getParameter("selectDelivery");
        String cartNumber = request.getParameter("cartNumber");
        String cartCCV2 = request.getParameter("cartCCV2");
        String cartDateMonth = request.getParameter("cartDateMonth");
        String cartDateYear = request.getParameter("cartDateYear");
        StringBuilder sb = new StringBuilder().append(currentOrder.getOrderDescription());
        sb.append(", Delivery type:" + selectedDelivery + ", Cart number:" + cartNumber + ", Cart CCV2:" + cartCCV2 + ", Cart date: " + cartDateMonth + "/" + cartDateYear);
        String description = String.valueOf(sb);
        Order order = new Order(description, currentOrder.getNumberOfProducts(), currentOrder.getPrice());
        User user = (User) request.getSession().getAttribute("userLogin");
        OrderRepository orderRepository = (OrderRepository) request.getServletContext().getAttribute("orderRepository");
        OrderInfoRepository orderInfoRepository = (OrderInfoRepository) request.getServletContext().getAttribute("orderInfoRepository");
        orderRepository.addOrder(order);
        String status = "wait";
        String statusDescription = "wait for check";
        String date = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        long userId = user.getId();
        List<Order> orderList = orderRepository.getAll();
        OrderInfo orderInfo = new OrderInfo(status, statusDescription, date, userId, orderList);
        orderInfoRepository.addOrderInfo(orderInfo);
        request.setAttribute("orderStatus", orderInfo.getOrderStatus());
        request.setAttribute("statusDescription", orderInfo.getStatusDescription());
        request.setAttribute("date", orderInfo.getDate());
        request.setAttribute("orderList", orderInfo.getOrderList());
        return ORDER_PAGE;
    }
}