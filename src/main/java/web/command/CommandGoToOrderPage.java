package web.command;

import entity.Item;
import entity.User;
import entity.order.Order;
import entity.order.OrderInfo;
import repository.order.OrderInfoRepository;
import service.Cart;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import static utils.Path.ORDER_PAGE;

public class CommandGoToOrderPage extends Command {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
        if (request.getSession().getAttribute("userLogin") == null) {
            String message = "you must register first";
            String registerButton = "registration";
            request.setAttribute("orderMessage", message);
            request.setAttribute("registration", registerButton);
            return ORDER_PAGE;
        }
        Cart cart = (Cart) request.getSession().getAttribute("cart");
        StringBuilder stringBuilder = new StringBuilder();
        for (Item item : cart.getListOfAllProductsInCart()) {
            stringBuilder.append(item.getName() + " " + item.getPrice() + "USD ");
        }
        String orderDescription = String.valueOf(stringBuilder);
        int numberOfProducts = cart.getNumberOfAllProductsInCart();
        int fullPrice = cart.getFullPrice();

        request.setAttribute("orderDescription", orderDescription);
        request.setAttribute("numberOfProducts", numberOfProducts);
        request.setAttribute("price", fullPrice);
        Order order = new Order(orderDescription, numberOfProducts, fullPrice);
        request.getSession().setAttribute("currentOrder", order);
        User user = (User) request.getSession().getAttribute("userLogin");
        long userId = user.getId();
        OrderInfoRepository orderInfoRepository = (OrderInfoRepository) request.getServletContext().getAttribute("orderInfoRepository");
        OrderInfo orderInfo = orderInfoRepository.getOrderInfoByUserId(userId);
        request.setAttribute("orderStatus", orderInfo.getOrderStatus());
        request.setAttribute("statusDescription", orderInfo.getStatusDescription());
        request.setAttribute("date", orderInfo.getDate());
        request.setAttribute("orderList", orderInfo.getOrderList());
        return ORDER_PAGE;
    }
}