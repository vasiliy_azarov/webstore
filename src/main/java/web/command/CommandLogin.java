package web.command;

import entity.Item;
import repository.DaoItem;

import repository.ItemRepository;
import service.Cart;
import utils.validators.LoginValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;

import static utils.Path.*;

public class CommandLogin extends Command {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String email = request.getParameter("user");
        String password = request.getParameter("pass");
        String defaultOrder = "name(A-Z)";
        Cart cart;
        if (request.getSession().getAttribute("cart") == null) {
            ItemRepository itemRepository = (ItemRepository) request.getServletContext().getAttribute("itemRepository");
            cart = new Cart(itemRepository);
        } else {
            cart = (Cart) request.getSession().getAttribute("cart");
        }
        LoginValidator loginValidator = new LoginValidator(email, password);
        if (loginValidator.singInValidation()) {
            request.getSession().setAttribute("userLogin", loginValidator.getLoggedInUser());
            DaoItem daoItem = (DaoItem) request.getServletContext().getAttribute("itemRepository");
            List<Item> itemList = daoItem.getAll();
            request.setAttribute("itemList", itemList);
            request.getSession().setAttribute("selectedOrder", defaultOrder);
            request.setAttribute("selectButton", "submit");
            request.getSession().setAttribute("itemList", itemList);
            request.setAttribute("numberOfProductsInCart", cart.getNumberOfAllProductsInCart());
            request.setAttribute("totalPrice", cart.getFullPrice());
            return CATALOG_PAGE;
        }
        request.setAttribute("message", loginValidator.getSingInErrorMessage());
        return LOGIN_PAGE;
    }
}