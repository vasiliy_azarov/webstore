package web.command;

import entity.Item;
import service.Cart;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static utils.Path.CART_PAGE;

public class CommandChangeNumberOfProduct extends Command {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
        Cart cart = (Cart) request.getSession().getAttribute("cart");
        if (request.getParameter("addProduct") != null) {
            cart.addItemToCart(Integer.parseInt(request.getParameter("addProduct")));
        }
        if (request.getParameter("deleteProduct") != null) {
            cart.deleteItemFromCart(Integer.parseInt(request.getParameter("deleteProduct")));
        }
        if (request.getParameter("clearCart") != null) {
            cart.clearCart();
        }
        List<Item> cartList = cart.getListOfAllProductsInCart();
        request.setAttribute("totalPrice", cart.getFullPrice());
        request.setAttribute("cartList", cartList);
        return CART_PAGE;
    }
}