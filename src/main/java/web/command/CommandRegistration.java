package web.command;

import captcha.CaptchaSynchronizer;
import captcha.CaptchaTimer;
import entity.User;
import repository.DaoUser;
import utils.CaptchaModeSelector;
import utils.validators.RegistrationValidator;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.Map;

import static utils.Path.LOGIN_PAGE;
import static utils.Path.REGISTRATION_PAGE;

public class CommandRegistration extends Command {

    private static final int THREAD_SLEEP_TIME_IN_MILLISECONDS = 2700;// This time is needed to delete and re-create captcha

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        CaptchaModeSelector cm = new CaptchaModeSelector();
        CaptchaSynchronizer instance = CaptchaSynchronizer.getInstance();
        int expectedCaptchaValue = 0;
        if (cm.isCookie()) {
            Cookie[] cookies = request.getCookies();
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("expectedCaptchaValue")) {
                    expectedCaptchaValue = Integer.parseInt(cookie.getValue());
                }
            }
        } else {
            expectedCaptchaValue = instance.getCaptchaMap().get(request.getSession().getId()).getCaptchaValue();
        }
        String userName = request.getParameter("userName");
        String userSurname = request.getParameter("userSurname");
        String userEmail = request.getParameter("userEmail");
        String userPassword = request.getParameter("userPassword");
        String userConfirmPassword = request.getParameter("userConfirmPassword");
        String userCaptcha = request.getParameter("captcha");
        boolean userNewsletter = request.getParameterValues("userMailing2") != null;

        DaoUser userRepository = (DaoUser) request.getServletContext().getAttribute("userRepository");
        RegistrationValidator registrationValidator = new RegistrationValidator(userName, userSurname, userEmail, userPassword, userConfirmPassword, expectedCaptchaValue, userCaptcha);
        if (registrationValidator.validation()) {
            for (Part part : request.getParts()) {
                if (part.getName().equals("userAvatar")) {
                    part.write("avatar" + userEmail + ".jpg");
                }
            }
            userRepository.addUser(new User(userName, userSurname, userEmail, userPassword, "resources/userAvatarStorage/avatar" + userEmail + ".jpg", userNewsletter));
            return LOGIN_PAGE;
        } else {
            request.setAttribute("userName", userName);
            request.setAttribute("userSurname", userSurname);
            request.setAttribute("userEmail", userEmail);
            request.setAttribute("userPassword", userPassword);
            request.setAttribute("userConfirmPassword", userConfirmPassword);
            request.setAttribute("javaMessage", registrationValidator.getValidationErrorMessage());
            Map<String, CaptchaTimer> captchaMap = instance.getCaptchaMap();
            CaptchaTimer captchaTimer = captchaMap.get((request.getSession().getId()));
            captchaTimer.captchaIsTyped();
            captchaMap.remove((request.getSession().getId()));
            captchaTimer = new CaptchaTimer(request.getSession().getId());
            captchaMap.put(request.getSession().getId(), captchaTimer);
            captchaTimer.start();
            request.setAttribute("sessionId", request.getSession().getId());
            while (!captchaTimer.captchaIsCreated) {

                try {
                    Thread.sleep(THREAD_SLEEP_TIME_IN_MILLISECONDS);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return REGISTRATION_PAGE;
    }
}