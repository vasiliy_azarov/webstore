package web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static utils.Path.LOGIN_PAGE;

public class CommandLogout extends Command {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        request.getSession().setAttribute("userLogin", null);
        return LOGIN_PAGE;
    }
}