package web.command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Locale;

public class CommandChangeLanguage extends Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
        String page = request.getParameter("currentPage");
        if (request.getParameter("changeLang").equals("EN")) {
            request.getSession().setAttribute("lang", Locale.US);
        }
        if (request.getParameter("changeLang").equals("GE")) {
            request.getSession().setAttribute("lang", Locale.GERMANY);
        }
        if (request.getParameter("changeLang").equals("RU")) {
            request.getSession().setAttribute("lang", Locale.getDefault());
        }
        return page;
    }
}