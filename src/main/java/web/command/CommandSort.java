package web.command;

import repository.ItemRepository;
import service.Cart;
import service.Sorter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.Arrays;

import static utils.Path.CATALOG_PAGE;

public class CommandSort extends Command {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws SQLException {
        String selectedName = request.getParameter("nameSelect");
        String selectedMinPrice = request.getParameter("startPriceSelect");
        String selectedMaxPrice = request.getParameter("endPriceSelect");
        String selectedOrder;
        String selectedNumberOfItem = request.getParameter("numberOfItem");
        String selectedManufacturer = request.getParameter("manufacturerSelect");
        String message = "No Items found";
        int defaultNumberOfItemOnPage = 3;
        int itemLimit;
        if (!request.getParameter("orderSortSelect").equals("select order")) {
            request.getSession().setAttribute("selectedOrder", request.getParameter("orderSortSelect"));
        }
        if (selectedNumberOfItem.equals("")) {
            itemLimit = defaultNumberOfItemOnPage;
        } else {
            itemLimit = Integer.parseInt(selectedNumberOfItem);
        }
        Cart cart;
        if (request.getSession().getAttribute("cart") == null) {
            ItemRepository itemRepository = (ItemRepository) request.getServletContext().getAttribute("itemRepository");
            cart = new Cart(itemRepository);
        } else {
            cart = (Cart) request.getSession().getAttribute("cart");
        }
        selectedOrder = (String) request.getSession().getAttribute("selectedOrder");
        Sorter sorter = new Sorter(selectedName, selectedOrder, selectedMinPrice, selectedMaxPrice, selectedManufacturer, (ItemRepository) request.getServletContext().getAttribute("itemRepository"));
        request.setAttribute("nameSelect", selectedName);
        request.getSession().setAttribute("nameSelect", selectedName);
        request.setAttribute("orderSortSelect", selectedOrder);
        request.getSession().setAttribute("orderSortSelect", selectedOrder);
        request.setAttribute("startPriceSelect", selectedMinPrice);
        request.getSession().setAttribute("startPriceSelect", selectedMinPrice);
        request.setAttribute("endPriceSelect", selectedMaxPrice);
        request.getSession().setAttribute("endPriceSelect", selectedMaxPrice);
        request.setAttribute("manufacturerSelect", selectedManufacturer);
        request.getSession().setAttribute("manufacturerSelect", selectedManufacturer);
        request.setAttribute("selectedOrder", selectedOrder);
        request.getSession().setAttribute("selectedOrder", selectedOrder);
        request.setAttribute("numberOfItem", selectedNumberOfItem);
        request.getSession().setAttribute("numberOfItem", selectedNumberOfItem);
        if (request.getParameter("nextPage") != null) {
            if (request.getSession().getAttribute("skippedItem") == null && request.getSession().getAttribute("numberItemOnPage") == null) {
                request.getSession().setAttribute("skippedItem", 0);
                request.getSession().setAttribute("numberItemOnPage", itemLimit);
                int skippedItem = (int) request.getSession().getAttribute("skippedItem");
                int numberItemOnPage = (int) request.getSession().getAttribute("numberItemOnPage");
                request.setAttribute("skippedItem", sorter.getSortedItemList(skippedItem, numberItemOnPage));
                request.getSession().setAttribute("itemList", sorter.getSortedItemList(skippedItem, numberItemOnPage));
            } else {
                int skippedItem = (int) request.getSession().getAttribute("skippedItem");
                int numberItemOnPage = (int) request.getSession().getAttribute("numberItemOnPage");

                if (sorter.getSortedItemList(skippedItem + itemLimit, numberItemOnPage).isEmpty()) {
                    request.getSession().setAttribute("itemList", sorter.getSortedItemList(skippedItem, numberItemOnPage));
                    request.setAttribute("itemList", sorter.getSortedItemList(skippedItem, numberItemOnPage));
                    request.getSession().setAttribute("skippedItem", skippedItem);
                } else {
                    request.getSession().setAttribute("itemList", sorter.getSortedItemList(skippedItem, numberItemOnPage));
                    request.setAttribute("itemList", sorter.getSortedItemList(skippedItem + itemLimit, numberItemOnPage));
                    request.getSession().setAttribute("skippedItem", skippedItem + itemLimit);
                }
            }
            if (Arrays.asList(request.getParameter("itemList")).get(0) == null) {
                request.setAttribute("messageItem", message);
            } else {
                request.setAttribute("messageItem", "");
            }
            request.setAttribute("numberOfProductsInCart", cart.getNumberOfAllProductsInCart());
            request.setAttribute("totalPrice", cart.getFullPrice());

            return CATALOG_PAGE;
        }
        if (request.getParameter("previousPage") != null) {
            if (request.getSession().getAttribute("skippedItem") == null && request.getSession().getAttribute("numberItemOnPage") == null) {
                request.getSession().setAttribute("skippedItem", 0);
                request.getSession().setAttribute("numberItemOnPage", itemLimit);
                int skippedItem = (int) request.getSession().getAttribute("skippedItem");
                int numberItemOnPage = (int) request.getSession().getAttribute("numberItemOnPage");
                request.getSession().setAttribute("itemList", sorter.getSortedItemList(skippedItem, numberItemOnPage));
                request.setAttribute("skippedItem", sorter.getSortedItemList(skippedItem, numberItemOnPage));
            } else {
                int skippedItem = (int) request.getSession().getAttribute("skippedItem");
                int numberItemOnPage = (int) request.getSession().getAttribute("numberItemOnPage");
                if (skippedItem - itemLimit < 0) {
                    request.getSession().setAttribute("itemList", sorter.getSortedItemList(skippedItem, numberItemOnPage));
                    request.setAttribute("skippedItem", sorter.getSortedItemList(skippedItem, numberItemOnPage));
                } else {
                    request.setAttribute("itemList", sorter.getSortedItemList(skippedItem - itemLimit, numberItemOnPage));
                    request.getSession().setAttribute("itemList", sorter.getSortedItemList(skippedItem, numberItemOnPage));
                    request.getSession().setAttribute("skippedItem", skippedItem - itemLimit);
                }
            }
            if (Arrays.asList(request.getParameter("itemList")).get(0) == null) {
                request.setAttribute("messageItem", message);
            } else {
                request.setAttribute("messageItem", "");
            }
            request.setAttribute("numberOfProductsInCart", cart.getNumberOfAllProductsInCart());
            request.setAttribute("totalPrice", cart.getFullPrice());

            return CATALOG_PAGE;
        }
        request.setAttribute("itemList", sorter.getSortedItemList(0, itemLimit));
        request.getSession().setAttribute("itemList", sorter.getSortedItemList(0, itemLimit));
        request.getSession().setAttribute("skippedItem", null);
        request.getSession().setAttribute("numberItemOnPage", null);
        if (Arrays.asList(request.getParameter("itemList")).get(0) == null) {
            request.setAttribute("messageItem", message);
        } else {
            request.setAttribute("messageItem", "");
        }
        request.setAttribute("numberOfProductsInCart", cart.getNumberOfAllProductsInCart());
        request.setAttribute("totalPrice", cart.getFullPrice());

        return CATALOG_PAGE;
    }
}