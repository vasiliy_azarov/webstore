package web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static utils.Path.LOGIN_PAGE;

public class CommandGoToLoginPage extends Command {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        return LOGIN_PAGE;
    }
}