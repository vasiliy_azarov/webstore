package web.controller;

import utils.Path;
import web.command.Command;
import web.command.CommandContainer;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@MultipartConfig(location = Path.AVATAR_REPOSITORY)
public class MainServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    protected void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String commandName = req.getParameter("command");
        Command com = CommandContainer.get(commandName);
        String forward;
        try {
            forward = com.execute(req, resp);
        } catch (Exception ex) {
            ex.printStackTrace();
            forward = "looks like this code was written by junior";
            req.setAttribute("message", ex.getMessage());
        }
        req.getRequestDispatcher(forward).forward(req, resp);
    }
}