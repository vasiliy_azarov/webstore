package web.listener;

import repository.ItemRepository;
import repository.RoleRepository;
import repository.UserRepository;
import repository.order.OrderInfoRepository;
import repository.order.OrderRepository;
import utils.SecureXMLDomParser;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext servletContext = servletContextEvent.getServletContext();
        servletContextEvent.getServletContext().setAttribute("userRepository", new UserRepository());
        servletContextEvent.getServletContext().setAttribute("itemRepository", new ItemRepository());
        servletContextEvent.getServletContext().setAttribute("orderInfoRepository", new OrderInfoRepository());
        servletContextEvent.getServletContext().setAttribute("orderRepository", new OrderRepository());
        servletContextEvent.getServletContext().setAttribute("roleRepository", new RoleRepository());

        String secureXmlPath = servletContext.getInitParameter("SecureXmlPath");
        SecureXMLDomParser secureXmlDomParser = new SecureXMLDomParser(secureXmlPath);
        servletContext.setAttribute("roleUriMap", secureXmlDomParser.read());

    }


    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        System.out.println("ServletContextListener was destroyed!");
    }
}