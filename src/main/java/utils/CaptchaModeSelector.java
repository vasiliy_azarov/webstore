package utils;

import java.util.ResourceBundle;

public class CaptchaModeSelector {

    public boolean isCookie() {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("captchaSelector");
        String mode = (String) resourceBundle.getObject(resourceBundle.getKeys().nextElement());
        return !mode.equals("session");
    }
}