package utils;

public class Path {
    public final static String LOGIN_PAGE = "/page/index.jsp";
    public final static String REGISTRATION_PAGE = "/page/registration.jsp";
    public final static String CATALOG_PAGE = "/page/catalog.jsp";
    public final static String CART_PAGE = "/page/cart.jsp";
    public final static String ORDER_PAGE = "/page/order.jsp";
    public final static String ERROR_PAGE = "/page/error.jsp";
    public final static String AVATAR_REPOSITORY = "C:/Users/Василий/Desktop/javaPreProd/webstore/src/main/webapp/resources/userAvatarStorage";
}