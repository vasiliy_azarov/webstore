package utils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class SecureXMLDomParser {
    private String file;

    public SecureXMLDomParser(String file) {
        this.file = file;
    }

    public Map<String, String> read() {
        Document document = getDocument();
        Map<String, String> roleUri = new HashMap<>();
        NodeList list = document.getElementsByTagName("constraint");

        fillingMap(roleUri, list);
        return roleUri;
    }

    private void fillingMap(Map<String, String> roleUri, NodeList list) {
        for (int index = 0; index < list.getLength(); index++) {
            Node node = list.item(index);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                String url = element.getElementsByTagName("url-pattern").item(0).getTextContent();
                String role = String.valueOf(element.getElementsByTagName("role").item(0).getTextContent());
                roleUri.put(url, role);
            }
        }
    }

    private Document getDocument() {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder;
        Document document = null;
        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
            document = documentBuilder.parse(new File(file));
            document.getDocumentElement().normalize();
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }
        return document;
    }
}

