package utils.validators;

import entity.User;
import repository.DaoUser;
import repository.UserRepository;

import java.util.List;

public class LoginValidator {
    private String email;
    private String password;
    private DaoUser userRepository = new UserRepository();
    private List<User> userList = userRepository.getAll();
    private StringBuilder singInErrorMessage = new StringBuilder();
    private User user = null;
    private String loginMessageText = "Invalid login or password";

    public LoginValidator(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public StringBuilder getSingInErrorMessage() {
        return singInErrorMessage;
    }

    public boolean singInValidation() {
        for (User user : userList) {
            if (user.getEmail().equals(email) && user.getPassword().equals(password)) {
                this.user = user;
                return true;
            }
        }
        singInErrorMessage.append(loginMessageText);
        return false;
    }

    public User getLoggedInUser() {
        return user;
    }
}