package utils.validators;

import entity.User;
import repository.DaoUser;
import repository.UserRepository;

import java.util.List;

public class RegistrationValidator {
    private final String userName;
    private final String userSurName;
    private final String userEmail;
    private final String userPassword;
    private final String userConfirmPassword;
    private final int expectedCaptchaValue;
    private final String currentCaptchaValue;
    private final StringBuilder validationErrorMessage = new StringBuilder().append("Wrong format of: ");

    public RegistrationValidator(String userName, String userSurName, String userEmail, String userPassword, String userConfirmPassword, int expectedCaptchaValue, String currentCaptchaValue) {
        this.userName = userName;
        this.userSurName = userSurName;
        this.userEmail = userEmail;
        this.userPassword = userPassword;
        this.userConfirmPassword = userConfirmPassword;
        this.expectedCaptchaValue = expectedCaptchaValue;
        this.currentCaptchaValue = currentCaptchaValue;
    }

    public StringBuilder getValidationErrorMessage() {
        return validationErrorMessage;
    }

    public boolean emailValidation() {
        DaoUser daoUser = new UserRepository();
        List<User> userList = daoUser.getAll();
        for (User user : userList) {
            if (user.getEmail().equals(userEmail)) {
                validationErrorMessage.append("email already exists; ");
                return false;
            }
        }
        return true;
    }

    public boolean passwordValidation() {
        if (userPassword.equals(userConfirmPassword)) {
            return true;
        }
        validationErrorMessage.append("password; ");
        return false;
    }

    public boolean nameValidation() {
        if (userName.length() > 2 && userName.length() < 16) {
            return true;
        }
        validationErrorMessage.append("name; ");
        return false;
    }

    public boolean surNameValidation() {
        if (userSurName.length() > 2 && userSurName.length() < 16) {
            return true;
        }
        validationErrorMessage.append("surname; ");
        return false;
    }

    public boolean captchaValidation() {
        int integerCurrentCaptchaValue;
        try {
            integerCurrentCaptchaValue = Integer.parseInt(currentCaptchaValue);
        } catch (Exception ex) {
            validationErrorMessage.append("captcha; ");
            return false;
        }
        if (integerCurrentCaptchaValue == expectedCaptchaValue) {
            return true;
        }
        validationErrorMessage.append("captcha; ");
        return false;
    }

    public boolean validation() {
        return emailValidation() && passwordValidation() && nameValidation() && surNameValidation() && captchaValidation();
    }
}