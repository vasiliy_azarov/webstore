package entity.order;

import java.util.Objects;

public class Order {
    private long id;
    private final String orderDescription;
    private final int numberOfProducts;
    private final int price;


    public Order(long id, String orderDescription, int numberOfProducts, int price) {
        this.id = id;
        this.orderDescription = orderDescription;
        this.numberOfProducts = numberOfProducts;
        this.price = price;
    }

    public Order(String orderDescription, int numberOfProducts, int price) {
        this.orderDescription = orderDescription;
        this.numberOfProducts = numberOfProducts;
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public String getOrderDescription() {
        return orderDescription;
    }

    public int getNumberOfProducts() {
        return numberOfProducts;
    }

    public int getPrice() {
        return price;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id && numberOfProducts == order.numberOfProducts && price == order.price && Objects.equals(orderDescription, order.orderDescription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, orderDescription, numberOfProducts, price);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", orderDescription='" + orderDescription + '\'' +
                ", numberOfProducts=" + numberOfProducts +
                ", price=" + price +
                '}';
    }
}