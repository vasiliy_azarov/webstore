package entity.order;

import java.util.List;
import java.util.Objects;

public class OrderInfo {
    private long id;
    private String orderStatus;
    private String statusDescription;
    private String date;
    private long userId;
    private List<Order> orderList;

    public OrderInfo(long id, String orderStatus, String statusDescription, String date, long userId, List<Order> orderList) {
        this.id = id;
        this.orderStatus = orderStatus;
        this.statusDescription = statusDescription;
        this.date = date;
        this.userId = userId;
        this.orderList = orderList;
    }

    public OrderInfo(String orderStatus, String statusDescription, String date, long userId, List<Order> orderList) {
        this.orderStatus = orderStatus;
        this.statusDescription = statusDescription;
        this.date = date;
        this.userId = userId;
        this.orderList = orderList;
    }

    @Override
    public String toString() {
        return "OrderInfo{" +
                "id=" + id +
                ", orderStatus='" + orderStatus + '\'' +
                ", statusDescription='" + statusDescription + '\'' +
                ", date='" + date + '\'' +
                ", userId=" + userId +
                ", orderList=" + orderList +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderInfo orderInfo = (OrderInfo) o;
        return id == orderInfo.id && userId == orderInfo.userId && orderList == orderInfo.orderList && Objects.equals(orderStatus, orderInfo.orderStatus) && Objects.equals(statusDescription, orderInfo.statusDescription) && Objects.equals(date, orderInfo.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, orderStatus, statusDescription, date, userId, orderList);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public List<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }
}