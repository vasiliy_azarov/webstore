package entity;

public class User {
    private long id;
    private String name;
    private String surname;
    private String email;
    private String password;
    private String avatarPath;
    private boolean newsletters;

    public User(long id, String name, String surname, String email, String password, String avatarPath, boolean newsletters) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.avatarPath = avatarPath;
        this.newsletters = newsletters;
    }

    public User(String name, String surname, String email, String password, String avatarPath, boolean newsletters) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.avatarPath = avatarPath;
        this.newsletters = newsletters;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", avatarPath='" + avatarPath + '\'' +
                ", newsletters='" + newsletters + '\'' +
                '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAvatarPath(String avatarPath) {
        this.avatarPath = avatarPath;
    }

    public String getAvatarPath() {
        return avatarPath;
    }

    public void setNewsletters(boolean newsletters) {
        this.newsletters = newsletters;
    }

    public boolean getNewsletters() {
        return newsletters;
    }
}