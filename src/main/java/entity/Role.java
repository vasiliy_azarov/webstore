package entity;

public class Role {
    int id;
    String role;
    int userId;

    public Role(String role, int userId) {
        this.role = role;
        this.userId = userId;
    }

    public Role(int id, String role, int userId) {
        this.id = id;
        this.role = role;
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}