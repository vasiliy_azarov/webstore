# SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
# SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
# SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE =
#         'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';
#
# DROP DATABASE `webstoredb`;
# CREATE SCHEMA IF NOT EXISTS `webStoreDB` DEFAULT CHARACTER SET utf8;
# USE `webstoredb`;
#
#
# CREATE TABLE IF NOT EXISTS `webstoredb`.`user`
# (
#     `id`          INT          NOT NULL AUTO_INCREMENT,
#     `name`        VARCHAR(16)  NOT NULL,
#     `surname`     VARCHAR(16)  NOT NULL,
#     `email`       VARCHAR(45)  NOT NULL,
#     `password`    VARCHAR(32)  NOT NULL,
#     `avatarPath`  VARCHAR(150) NOT NULL,
#     `newsletters` TINYINT(1)   NOT NULL,
#     PRIMARY KEY (`id`),
#     UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE
# )
#     ENGINE = InnoDB;
#
#
# CREATE TABLE IF NOT EXISTS `webstoredb`.`item`
# (
#     `id`           INT          NOT NULL AUTO_INCREMENT,
#     `name`         VARCHAR(40)  NOT NULL,
#     `price`        INT          NOT NULL,
#     `quantity`     INT          NOT NULL,
#     `manufacturer` VARCHAR(40)  NOT NULL,
#     `description`  VARCHAR(200) NOT NULL,
#     PRIMARY KEY (`id`),
#     UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE
# )
#     ENGINE = InnoDB;
#
#
# CREATE TABLE IF NOT EXISTS `webstoredb`.`order_info`
# (
#     `id`                 INT          NOT NULL AUTO_INCREMENT,
#     `order_status`       VARCHAR(50)  NOT NULL,
#     `status_description` VARCHAR(200) NOT NULL,
#     `date`               VARCHAR(30)  NOT NULL,
#     `user_id`            INT          NOT NULL,
#     `order_list`         VARCHAR(200) NOT NULL,
#     PRIMARY KEY (`id`),
#     UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE
# )
#     ENGINE = InnoDB;
#
# CREATE TABLE IF NOT EXISTS `webstoredb`.`product_order`
# (
#     `id`                 INT          NOT NULL AUTO_INCREMENT,
#     `order_description`  VARCHAR(200) NOT NULL,
#     `number_of_products` INT          NOT NULL,
#     `price`              INT          NOT NULL,
#     PRIMARY KEY (`id`),
#     UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE
# )
#     ENGINE = InnoDB;
#
# CREATE TABLE IF NOT EXISTS `webstoredb`.`role`
# (
#     `user_id` INT          NOT NULL AUTO_INCREMENT,
#     `role`    VARCHAR(200) NOT NULL,
#     UNIQUE INDEX `id_UNIQUE` (`user_id` ASC) VISIBLE
# )
#     ENGINE = InnoDB;
#
#
# INSERT role(user_id, role) VALUE (1, 'user');
# INSERT role(user_id, role) VALUE (2, 'admin');
#
# INSERT product_order(order_description, number_of_products, price) VALUE ('bla bla bla description', 1, 1111);
# INSERT product_order(order_description, number_of_products, price) VALUE ('bla bla bla description', 2, 2222);
#
#
# INSERT order_info(order_status, status_description, date, user_id, order_list) VALUE ('wait',
#                                                                                       'bla bla status description',
#                                                                                       '12.12.2020', 1, '1, 2, 3, 4, 5');
# INSERT order_info(order_status, status_description, date, user_id, order_list) VALUE ('cancel',
#                                                                                       'bla bla status description',
#                                                                                       '01.01.2021', 2,
#                                                                                       '11, 12, 13, 14, 15');
#
#
# INSERT user(name, surname, email, password, avatarPath, newsletters) VALUE ('userName1', 'userSurname1',
#                                                                             'userEmail1@email.com', 'user1password',
#                                                                             '/////', true);
# INSERT user(name, surname, email, password, avatarPath, newsletters) VALUE ('userName2', 'userSurname2',
#                                                                             'userEmail2@email.com', 'user2password',
#                                                                             '/////', false);
#
#
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name1', 111, 1, 'China', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name2', 111, 1, 'Poland', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name3', 222, 1, 'Poland', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name4', 222, 1, 'USA', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name5', 333, 1, 'Ukraine', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name6', 333, 1, 'China', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name7', 111, 1, 'USA', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name8', 111, 1, 'China', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name9', 111, 1, 'Poland', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name10', 222, 1, 'Poland', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name11', 222, 1, 'USA', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name12', 555, 1, 'Ukraine', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name13', 555, 1, 'China', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name14', 666, 1, 'USA', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name15', 666, 1, 'China', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name16', 111, 1, 'Poland', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name17', 222, 1, 'Poland', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name18', 444, 1, 'USA', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name19', 333, 1, 'Ukraine', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name20', 444, 1, 'China', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name21', 222, 1, 'USA', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name22', 444, 1, 'China', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name23', 777, 1, 'Poland', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name24', 555, 1, 'Poland', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name25', 333, 1, 'USA', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name26', 888, 1, 'Ukraine', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name27', 222, 1, 'China', 'bla bla bla bla');
# INSERT item(name, price, quantity, manufacturer, description) VALUE ('name28', 666, 1, 'USA', 'bla bla bla bla');
#
# SET SQL_MODE = @OLD_SQL_MODE;
# SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
# SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;
#

-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE =
        'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema webstoredb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema webstoredb
-- -----------------------------------------------------
DROP DATABASE `webstoredb`;
CREATE SCHEMA IF NOT EXISTS `webstoredb` DEFAULT CHARACTER SET utf8;
USE `webstoredb`;

-- -----------------------------------------------------
-- Table `webstoredb`.`item`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webstoredb`.`item`
(
    `id`           INT          NOT NULL AUTO_INCREMENT,
    `name`         VARCHAR(40)  NOT NULL,
    `price`        INT          NOT NULL,
    `quantity`     INT          NOT NULL,
    `manufacturer` VARCHAR(40)  NOT NULL,
    `description`  VARCHAR(200) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 29
    DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `webstoredb`.`order_info`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webstoredb`.`order_info`
(
    `id`                 INT          NOT NULL AUTO_INCREMENT,
    `order_status`       VARCHAR(50)  NOT NULL,
    `status_description` VARCHAR(200) NOT NULL,
    `date`               VARCHAR(30)  NOT NULL,
    `user_id`            INT          NOT NULL,
    `order_list`         VARCHAR(200) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 3
    DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `webstoredb`.`product_order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webstoredb`.`product_order`
(
    `id`                 INT          NOT NULL AUTO_INCREMENT,
    `order_description`  VARCHAR(200) NOT NULL,
    `number_of_products` INT          NOT NULL,
    `price`              INT          NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 3
    DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `webstoredb`.`role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webstoredb`.`role`
(
    `id`      INT          NOT NULL AUTO_INCREMENT,
    `role`    VARCHAR(200) NOT NULL,
    `user_id` INT          NOT NULL,
    UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 3
    DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `webstoredb`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webstoredb`.`user`
(
    `id`          INT          NOT NULL AUTO_INCREMENT,
    `name`        VARCHAR(16)  NOT NULL,
    `surname`     VARCHAR(16)  NOT NULL,
    `email`       VARCHAR(45)  NOT NULL,
    `password`    VARCHAR(32)  NOT NULL,
    `avatarPath`  VARCHAR(150) NOT NULL,
    `newsletters` TINYINT(1)   NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 1
    DEFAULT CHARACTER SET = utf8mb3;



INSERT role(role, user_id) VALUE ('user', 1);
INSERT role(role, user_id) VALUE ('admin', 2);


INSERT product_order(order_description, number_of_products, price) VALUE ('bla bla bla description', 1, 1111);
INSERT product_order(order_description, number_of_products, price) VALUE ('bla bla bla description', 2, 2222);


INSERT order_info(order_status, status_description, date, user_id, order_list) VALUE ('wait',
                                                                                      'bla bla status description',
                                                                                      '12.12.2020', 1, '1, 2, 3, 4, 5');
INSERT order_info(order_status, status_description, date, user_id, order_list) VALUE ('cancel',
                                                                                      'bla bla status description',
                                                                                      '01.01.2021', 2,
                                                                                      '11, 12, 13, 14, 15');


INSERT user(name, surname, email, password, avatarPath, newsletters) VALUE ('userName1', 'userSurname1',
                                                                            'userEmail1@email.com', 'user1password',
                                                                            '/////', true);
INSERT user(name, surname, email, password, avatarPath, newsletters) VALUE ('userName2', 'userSurname2',
                                                                            'userEmail2@email.com', 'user2password',
                                                                            '/////', false);

INSERT item(name, price, quantity, manufacturer, description) VALUE ('name1', 111, 1, 'China', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name2', 111, 1, 'Poland', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name3', 222, 1, 'Poland', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name4', 222, 1, 'USA', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name5', 333, 1, 'Ukraine', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name6', 333, 1, 'China', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name7', 111, 1, 'USA', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name8', 111, 1, 'China', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name9', 111, 1, 'Poland', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name10', 222, 1, 'Poland', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name11', 222, 1, 'USA', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name12', 555, 1, 'Ukraine', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name13', 555, 1, 'China', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name14', 666, 1, 'USA', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name15', 666, 1, 'China', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name16', 111, 1, 'Poland', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name17', 222, 1, 'Poland', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name18', 444, 1, 'USA', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name19', 333, 1, 'Ukraine', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name20', 444, 1, 'China', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name21', 222, 1, 'USA', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name22', 444, 1, 'China', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name23', 777, 1, 'Poland', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name24', 555, 1, 'Poland', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name25', 333, 1, 'USA', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name26', 888, 1, 'Ukraine', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name27', 222, 1, 'China', 'bla bla bla bla');
INSERT item(name, price, quantity, manufacturer, description) VALUE ('name28', 666, 1, 'USA', 'bla bla bla bla');


SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;
