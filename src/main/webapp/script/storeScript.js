const BTN = document.getElementById("btn");
const INPUT_LOGIN = document.getElementById("user");
const INPUT_PASSWORD = document.getElementById("pass");
const MESSAGE = document.getElementById("message");
const REG_EX_EMAIL = /.+@.+\../;
const REG_EX_PASSWORD_BY_LIT  = /[A-Za-z]/;
const REG_EX_PASSWORD_BY_NUM = /[0-9]/;
const REG_EX_PASSWORD_BY_SYMBOL = /[^A-Za-z0-9]/;
const LOGIN_FORM = document.getElementById("wrapper");
let validPasswordByLength = false;
let passwordValid = false;
let emailValid = false;

BTN.onclick = function(){
    let validEmail = REG_EX_EMAIL.exec(INPUT_LOGIN.value);
    if(validEmail ) {
        emailValid = true;
        let validPasswordByLit = REG_EX_PASSWORD_BY_LIT.exec(INPUT_PASSWORD.value);
        let validPasswordByNum = REG_EX_PASSWORD_BY_NUM.exec(INPUT_PASSWORD.value);
        let validPasswordBySymbol = REG_EX_PASSWORD_BY_SYMBOL.exec(INPUT_PASSWORD.value);
        if(INPUT_PASSWORD.value.length>8 && INPUT_PASSWORD.value.length<32){
            validPasswordByLength = true;
        }
        if(validPasswordByLit && validPasswordByNum && !validPasswordBySymbol && validPasswordByLength){
            passwordValid = true;
        }
        else {
            passwordValid= false;
            LOGIN_FORM.addEventListener("submit", (event)=>{
                event.preventDefault();
            });
            MESSAGE.innerHTML = "Invalid password format";
        }
    }
    else {
        emailValid = false;
        LOGIN_FORM.addEventListener("submit", (event)=>{
            event.preventDefault();
        });
        MESSAGE.innerHTML = "Invalid email format";
    }
    if(passwordValid && emailValid){
        MESSAGE.style.visibility="hidden";
    }

}