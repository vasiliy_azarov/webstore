const BTN = $("#btn");
const INPUT_LOGIN = $("#user");
const INPUT_PASSWORD = $("#pass");
const MESSAGE = $("#message");
const REG_EX_EMAIL = /.+@.+\../;
const REG_EX_PASSWORD_BY_LIT = /[A-Za-z]/;
const REG_EX_PASSWORD_BY_NUM = /[0-9]/;
const REG_EX_PASSWORD_BY_SYMBOL = /[^A-Za-z0-9]/;
let validPasswordByLength = false;
let passwordValid = false;
let emailValid = false;

$(BTN).click(function () {
    let validEmail = REG_EX_EMAIL.exec(INPUT_LOGIN.val());

    if (validEmail) {
        emailValid = true;
        let validPasswordByLit = REG_EX_PASSWORD_BY_LIT.exec(INPUT_PASSWORD.val());
        let validPasswordByNum = REG_EX_PASSWORD_BY_NUM.exec(INPUT_PASSWORD.val());
        let validPasswordBySymbol = REG_EX_PASSWORD_BY_SYMBOL.exec(INPUT_PASSWORD.val());

        if (INPUT_PASSWORD.val().length > 8 && INPUT_PASSWORD.val().length < 32) {
            validPasswordByLength = true;
        }
        if (validPasswordByLit && validPasswordByNum && !validPasswordBySymbol && validPasswordByLength) {
            passwordValid = true;
        } else {
            passwordValid = false;
            MESSAGE.html("Invalid password format");
        }
    } else {
        emailValid = false;
        MESSAGE.html("Invalid email format");
    }
    if (passwordValid && emailValid) {
        window.location.href = 'page/catalog.html';
    }
})