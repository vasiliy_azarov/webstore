const USER_NAME = document.getElementById("userName");
const USER_SURNAME = document.getElementById("userSurname");
const USER_EMAIL = document.getElementById("userEmail");
const USER_PASSWORD = document.getElementById("userPassword");
const USER_CONFIRM_PASSWORD = document.getElementById("userConfirmPassword");
const USER_CONFIRM_REGISTRATION = document.getElementById("userConfirmRegistration");
const MESSAGE = document.getElementById("message");
const REG_EX_EMAIL = /.+@.+\../;
const REG_EX_PASSWORD_BY_LIT = /[A-Za-z]/;
const REG_EX_PASSWORD_BY_NUM = /[0-9]/;
const REG_EX_PASSWORD_BY_SYMBOL = /[^A-Za-z0-9]/;
let validPasswordByLength = false;
let passwordValid = false;
let emailValid = false;
let userNameAndUserSurnameLength = false

USER_CONFIRM_REGISTRATION.onclick = function () {
    let validEmail = REG_EX_EMAIL.exec(USER_EMAIL.value);
    if (validEmail) {
        emailValid = true;
        let validPasswordByLit = REG_EX_PASSWORD_BY_LIT.exec(USER_PASSWORD.value);
        let validPasswordByNum = REG_EX_PASSWORD_BY_NUM.exec(USER_PASSWORD.value);
        let validPasswordBySymbol = REG_EX_PASSWORD_BY_SYMBOL.exec(USER_PASSWORD.value);
        if (USER_PASSWORD.value.length > 8 && USER_PASSWORD.value.length < 32) {
            validPasswordByLength = true;
        }
        if (validPasswordByLit && validPasswordByNum && !validPasswordBySymbol && validPasswordByLength) {
            passwordValid = true;
        } else {
            passwordValid = false;
            USER_CONFIRM_REGISTRATION.setAttribute("type", "button");
            MESSAGE.innerHTML = "Invalid password format";
        }
    } else {
        emailValid = false;
        USER_CONFIRM_REGISTRATION.setAttribute("type", "button");
        MESSAGE.innerHTML = "Invalid email format";
    }
    if (USER_PASSWORD.value != USER_CONFIRM_PASSWORD.value) {
        MESSAGE.innerHTML = "Passwords don`t match";
        USER_CONFIRM_REGISTRATION.setAttribute("type", "button");
    } else {
        if (USER_NAME.value.length >= 4 && USER_SURNAME.value.length >= 4) {
            userNameAndUserSurnameLength = true;
        } else {
            USER_CONFIRM_REGISTRATION.setAttribute("type", "button");
            MESSAGE.innerHTML = "Name and Surname should have minimum 4 symbols";
        }

    }
    if (emailValid && passwordValid && userNameAndUserSurnameLength) {
        MESSAGE.innerHTML = "";
        USER_CONFIRM_REGISTRATION.setAttribute("type", "submit");
    }
}