<%@ page import="java.util.Locale" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/customTag.tld" prefix="cpt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="shopLang"/>
<html lang="${lang}">
<head>
    <meta charset="UTF-8">
    <title>Web Store</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css">
</head>
<body>
<cpt:languageTag/>
<input type="hidden" name="currentPage" value="/page/catalog.jsp" form="changeLanguage">
<form id="goToLoginPage" method="post" action="MainServlet/index.jsp">
    <input type="hidden" name="command" value="GoToLoginPage">
    <button type="submit" id="goToLogin"><fmt:message key="label.backText"/></button>
</form>

<form id="goToCartPage" method="post" action="MainServlet/cart.jsp">
    <input type="hidden" name="command" value="GoToCartPage">
    <table id="table3" class="center" form="buyItem">
        <tr>
            <td>
                <button name="goToCart" type="submit" id="goToCart"><fmt:message key="label.cartText"/></button>
            </td>
            <td><fmt:message key="label.numberOfProductsInCartText"/> ${numberOfProductsInCart}</td>
            <td><fmt:message key="label.totalPriceText"/> ${totalPrice}</td>
        </tr>
    </table>
</form>
<form id="sort" name="sort" method="post" action="MainServlet/catalog.jsp"></form>
<form id="buyItem" name="buyItem" method="post" action="MainServlet/catalog.jsp"></form>
<input type="hidden" name="command" value="Sort" form="sort">
<input id="numberOfItem" name="numberOfItem" type="number" value="${numberOfItem}"
       placeholder=
       <fmt:message key="label.choiceNumberOfItemOnPageText"/> form="sort">
<select id="orderSortSelect" name="orderSortSelect" form="sort">
    <option>select order</option>
    <option>name(A-Z){}</option>
    <option>name(Z-A)</option>
    <option>price(waning)</option>
    <option>price(increase)</option>
    <option>manufacturer(A-Z)</option>
    <option>manufacturer(Z-A)</option>
</select>
<input id="nameSelect" name="nameSelect" value="${nameSelect}" placeholder=
<fmt:message key="label.typeProductNameText"/> form="sort">
<input id="startPriceSelect" name="startPriceSelect" type="number" value="${startPriceSelect}"
       placeholder=
       <fmt:message key="label.typeMinPriceText"/> form="sort">
<input id="endPriceSelect" name="endPriceSelect" type="number" value="${endPriceSelect}"
       placeholder=
       <fmt:message key="label.typeMaxPriceText"/> form="sort">
<input id="manufacturerSelect" name="manufacturerSelect" value="${manufacturerSelect}"
       placeholder=
       <fmt:message key="label.typeManufacturerText"/> form="sort">
<button type="submit" id="startSort" name="startSort" value="startSort" form="sort"><fmt:message
        key="label.sortText"/></button>
<input type="hidden" name="command" value="BuyItem" form="buyItem">
<table id="table1" class="center" form="buyItem">
    <tr>
        <th><fmt:message key="label.nameText"/></th>
        <th><fmt:message key="label.priceText"/></th>
        <th><fmt:message key="label.manufacturerText"/></th>
        <th><fmt:message key="label.descriptionText"/></th>
        <th><fmt:message key="label.buyText"/></th>
    </tr>
    <c:forEach var="item" items="${itemList}">
    <tr>
        <td>${item.name}</td>
        <td>${item.price}</td>
        <td>${item.manufacturer}</td>
        <td>${item.description}</td>
        <td>
            <button type="submit" name="buy" form="buyItem" value="${item.id}"><fmt:message
                    key="label.buyText"/></button>
        </td>
        </c:forEach>
    </tr>
</table>
<div id="message" form="sort">${messageItem}</div>
<button type="submit" id="previousPage" name="previousPage" value="nextPage" form="sort"><fmt:message
        key="label.previousPageText"/></button>
<button type="submit" id="nextPage" name="nextPage" value="nextPage" form="sort"><fmt:message
        key="label.nextPageText"/></button>
</form>
</body>
</html>