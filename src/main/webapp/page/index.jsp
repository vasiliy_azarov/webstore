<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/customTag.tld" prefix="cpt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="shopLang"/>
<html lang="${lang}">
<head>

    <meta charset="UTF-8">
    <title>Web Store</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<h1><fmt:message key="label.singInFormText"/></h1>
<cpt:languageTag/>
<input type="hidden" name="currentPage" value="/page/index.jsp" form="changeLanguage">
<form id="wrapper" method="post" action="MainServlet/catalog.jsp">
    <table id="table2" class="center">
        <th id="th2">
            <cpt:loginTag/>
            ${message}
        </th>
    </table>
</form>
<form id="GoToRegistrationPage" name="GoToRegistrationPage" method="post" action="MainServlet/registration.jsp">
    <input type="hidden" name="command" value="GoToRegistrationPage">
    <button type="submit" name="goToRegisterPage" id="goToRegisterPage" value="GoToRegistrationPage">
        <div><u><fmt:message key="label.registrationText"/></u></div>
    </button>
</form>
<script src="script/storeScript.js"></script>
</body>
</html>