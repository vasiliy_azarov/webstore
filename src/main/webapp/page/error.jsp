<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<h2>You do not have access to this page</h2>
<form id="GoToStartPage" name="GoToStartPage" method="post" action="index.jsp">
    <input type="hidden" name="command" value="GoToLoginPage">
    <button type="submit" name="GoToLoginPage" id="GoToLoginPage" value="GoToLoginPage">
        Login
    </button>
</form>
<form id="GoToRegistrPage" name="GoToRegistrationPage" method="post" action="registration.jsp">
    <input type="hidden" name="command" value="GoToRegistrationPage">
    <button type="submit" name="GoToRegistrationPage" id="GoToRegistrationPage" value="GoToRegistrationPage">
        Registration
    </button>
</form>
</body>
</html>
