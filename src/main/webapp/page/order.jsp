<%@ page import="java.util.Locale" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/customTag.tld" prefix="cpt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="shopLang"/>
<html lang="${lang}">
<head>
    <meta charset="UTF-8">
    <title>Web Store</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<cpt:languageTag/>
<input type="hidden" name="currentPage" value="/page/order.jsp" form="changeLanguage">
<form id="goToCartPage" method="post" action="MainServlet/cart.jsp">
    <input type="hidden" name="command" value="GoToCartPage">
    <button name="goToCart" type="submit" id="goToCart"><fmt:message key="label.cartText"/></button>
</form>
<h1>${yourCurrentOrderText}</h1>
<form id="createOrder" method="post" action="MainServlet/order.jsp">
    <input type="hidden" name="command" value="CreateOrder">
    <table id="table5" class="center">
        <tr>
            <th><fmt:message key="label.orderDescriptionText"/></th>
            <th><fmt:message key="label.numberOfProductsText"/></th>
            <th><fmt:message key="label.priceText"/></th>
        </tr>
        <tr>
            <td>${orderDescription}</td>
            <td>${numberOfProducts}</td>
            <td>${price}</td>
        </tr>
    </table>
    <select id="selectDelivery" name="selectDelivery">
        <option>choice delivery</option>
        <option>by courier</option>
        <option>to the post office</option>
        <option>to the point of issue</option>
    </select>
    <input type="number" id="cartNumber" placeholder=<fmt:message key="label.cardNumberPlaceholderText"/>>
    <input type="number" id="cartCCV2" placeholder=<fmt:message key="label.cardCCV2Text"/>>
    <input type="number" id="cartDateMonth" placeholder=<fmt:message key="label.cardMonthMMText"/>>
    <input type="number" id="cartDateYear" placeholder=<fmt:message key="label.cardYearYYText"/>>
    <button type="submit" id="createOrderBut" name="createOrderBut" value="createOrder"><fmt:message
            key="label.createOrderText"/></button>
</form>
<form id="GoToRegistrationPage" name="GoToRegistrationPage" method="post" action="MainServlet/registration.jsp">
    <input type="hidden" name="command" value="GoToRegistrationPage">
    <button type="submit" name="goToRegisterPage" id="goToRegisterPage" value="GoToRegistrationPage">
        <h2>${orderMessage}</h2>
        <div><u>${registration}</u></div>
    </button>
</form>
<table id="table3" class="center">
    <tr>
        <th><fmt:message key="label.orderStatusText"/></th>
        <th><fmt:message key="label.statusDescriptionText"/></th>
        <th><fmt:message key="label.dateText"/></th>
        <th><fmt:message key="label.orderListText"/></th>
    </tr>
    <tr>
        <td>${orderStatus}</td>
        <td>${statusDescription}</td>
        <td>${date}</td>
        <td><c:forEach var="order" items="${orderList}">
            ${order.orderDescription}
        </c:forEach>
        </td>
    </tr>
</table>
</body>
</html>