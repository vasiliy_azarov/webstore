<%@ page import="java.util.Locale" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/customTag.tld" prefix="cpt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="shopLang"/>
<html lang="${lang}">
<head>
    <meta charset="UTF-8">
    <title>Web Store</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css">
</head>
<body>
<h1><fmt:message key="label.registrationText"/></h1>
<cpt:languageTag/>
<input type="hidden" name="currentPage" value="/page/registration.jsp" form="changeLanguage">
<div id="registrationWrapper">
    <form id="registrationForm" method="post" action="MainServlet/catalog.jsp" enctype="multipart/form-data">
        <input type="hidden" name="command" value="Registration">
        <input id="userName" name="userName" type="text" placeholder=
        <fmt:message key="label.namePlaceholderText"/> value="${userName}">
        <input id="userSurname" name="userSurname" type="text" placeholder=
        <fmt:message key="label.surnamePlaceholderText"/> value="${userSurname}">
        <input id="userEmail" name="userEmail" type="text" placeholder=
        <fmt:message key="label.emailPlaceholderText"/> value="${userEmail}">
        <input id="userPassword" name="userPassword" type="password" placeholder=
        <fmt:message key="label.passwordPlaceholderText"/> value="${userPassword}">
        <input id="userConfirmPassword" name="userConfirmPassword" type="password" placeholder=
        <fmt:message key="label.passwordConfirmPlaceholderText"/>
                value="${userConfirmPassword}">
        <div id="avatarText" name="avatarText"><fmt:message key="label.downloadAvatarText"/></div>
        <div id="avatarDiv" name="avatarDiv"><input id="userAvatar" name="userAvatar" type="file" multiple></div>
        <input id="userMailing1" name="userMailing1" type="checkbox" value="true">
        <div id="checkbox-text"><fmt:message key="label.storeRulesAgreementText"/></div>
        <input id="userMailing2" name="userMailing2" type="checkbox">
        <div id="checkbox-text"><fmt:message key="label.receiveNewslettersText"/></div>
        <cpt:captchaTag/>
        <button type="submit" id="userConfirmRegistration"><fmt:message key="label.confirmText"/></button>
        <div id="message">${javaMessage}</div>
    </form>
</div>
<script src="${pageContext.request.contextPath}/script/registrationPageScript.js"></script>
</body>
</html>