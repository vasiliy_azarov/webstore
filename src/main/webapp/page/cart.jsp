<%@ page import="java.util.Locale" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/customTag.tld" prefix="cpt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="shopLang"/>
<html lang="${lang}">
<head>
    <meta charset="UTF-8">
    <title>Web Store</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<cpt:languageTag/>
<input type="hidden" name="currentPage" value="/page/cart.jsp" form="changeLanguage">
<form id="numberOfProduct" name="changeNumberOfProduct" method="post" action="MainServlet/cart.jsp">
    <input type="hidden" name="command" value="ChangeNumberOfProduct">
    <table id="table5" class="center">
        <tr>
            <th><fmt:message key="label.nameText"/></th>
            <th><fmt:message key="label.priceText"/></th>
            <th><fmt:message key="label.manufacturerText"/></th>
            <th><<fmt:message key="label.descriptionText"/></th>
            <th><fmt:message key="label.quantityText"/></th>
        </tr>
        <c:forEach var="item" items="${cartList}">
        <tr>
            <td>${item.name}</td>
            <td>${item.price}</td>
            <td>${item.manufacturer}</td>
            <td>${item.description}</td>
            <td>
                <button type="submit" name="addProduct" value="${item.id}">+</button>
                <button type="submit" name="deleteProduct" value="${item.id}">-</button>
            </td>
            </c:forEach>
        </tr>
        <tr>
            <td>${totalPriceText}</td>
            <td>${totalPrice}</td>
            <td></td>
            <td></td>
            <td>
                <button type="submit" id="clearCart" name="clearCart" value="clearCart"><fmt:message
                        key="label.clearCartText"/></button>
            </td>
        </tr>
    </table>
</form>
<form id="goToOrderPage" name="goToOrderPage" method="post" action="MainServlet/order.jsp">
    <input type="hidden" name="command" value="GoToOrderPage">
    <button type="submit" id="goToOrder" name="goToOrder"><fmt:message key="label.createOrderText"/></button>
</form>
</body>
</html>