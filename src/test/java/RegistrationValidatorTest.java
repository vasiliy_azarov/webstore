import entity.User;
import org.junit.Test;
import utils.validators.RegistrationValidator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RegistrationValidatorTest {

    final int expectedCaptcha = 4356;
    User validUniqueUser = new User(3, "userName3", "userSurname3", "userEmail3@email.com", "user3password", "///", true);
    User notValidUser = new User(4, "us", "us", "userEmail2@email.com", "user123123", "///", false);
    RegistrationValidator registrationValidationOnValidUser = new RegistrationValidator(validUniqueUser.getName(), validUniqueUser.getSurname(), validUniqueUser.getEmail(), validUniqueUser.getPassword(), validUniqueUser.getPassword(), expectedCaptcha, String.valueOf(4356));
    RegistrationValidator registrationValidationOnNotValidUser = new RegistrationValidator(notValidUser.getName(), notValidUser.getSurname(), notValidUser.getEmail(), notValidUser.getPassword(), "qweqwe123", expectedCaptcha, String.valueOf(1234));

    @Test
    public void emailValidationTestPassedIfValidationUserEmailReturnTrueOnUniqueEmailAndFalseOnDuplicateEmail() {
        assertTrue(registrationValidationOnValidUser.emailValidation());
        assertFalse(registrationValidationOnNotValidUser.emailValidation());
    }

    @Test
    public void passwordValidationTestPassedIfValidationUserPasswordReturnTrueOnEquivalentsPasswordsAndFalseOnDifferencesPasswords() {
        assertTrue(registrationValidationOnValidUser.passwordValidation());
        assertFalse(registrationValidationOnNotValidUser.passwordValidation());
    }

    @Test
    public void nameValidationTestPassedIfValidationUserNameReturnTrueOnNameThatLongerThanFourCharactersAndFalseOnLess() {
        assertTrue(registrationValidationOnValidUser.nameValidation());
        assertFalse(registrationValidationOnNotValidUser.nameValidation());
    }

    @Test
    public void surnameValidationTestPassedIfValidationUserSurnameReturnTrueOnSurnameThatLongerThanFourCharactersAndFalseOnLess() {
        assertTrue(registrationValidationOnValidUser.surNameValidation());
        assertFalse(registrationValidationOnNotValidUser.surNameValidation());
    }


    @Test
    public void captchaValidationTestPassedIfValidationUserCaptchaReturnTrueIfEnteredCaptchaMatchesEmailAndFalseOnDuplicateEmail() {
        assertTrue(registrationValidationOnValidUser.captchaValidation());
        assertFalse(registrationValidationOnNotValidUser.captchaValidation());
    }
}
