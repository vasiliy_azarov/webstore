import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import web.filters.LanguageFilter;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;


public class LanguageFilterTest {
    HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
    HttpServletResponse mockedResponse = Mockito.mock(HttpServletResponse.class);
    HttpSession mockHttpSession = Mockito.mock(HttpSession.class);
    FilterChain mockFilterChain = Mockito.mock(FilterChain.class);
    LanguageFilter languageFilter = new LanguageFilter();
    final Map<String, Object> attributes = new HashMap<>();
    final Map<String, Object> parameters = new HashMap<>();
    FilterConfig mockedFilterConfig = Mockito.mock(FilterConfig.class);

    @Before
    public void beforeTestsCreateMockedSessionAndInitParams() throws ServletException, IOException {
        Mockito.doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) {
                String key = (String) invocation.getArguments()[0];
                return attributes.get(key);
            }
        }).when(mockHttpSession).getAttribute(anyString());
        Mockito.doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) {
                String key = (String) invocation.getArguments()[0];
                Object value = invocation.getArguments()[1];
                attributes.put(key, value);
                return null;
            }
        }).when(mockHttpSession).setAttribute(anyString(), any());


        Mockito.doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) {
                String key = (String) invocation.getArguments()[0];
                return parameters.get(key);
            }
        }).when(mockedRequest).getAttribute(anyString());
        Mockito.doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) {
                String key = (String) invocation.getArguments()[0];
                Object value = invocation.getArguments()[1];
                parameters.put(key, value);
                return null;
            }
        }).when(mockedRequest).setAttribute(anyString(), any());
        Mockito.when(mockedRequest.getSession()).thenReturn(mockHttpSession);
        Mockito.when(mockedFilterConfig.getInitParameter("session")).thenReturn("true");
        Mockito.when(mockedFilterConfig.getInitParameter("cookie")).thenReturn("false");
        Mockito.when(mockedFilterConfig.getInitParameter("cookieLifeInDays")).thenReturn("30");
        languageFilter.init(mockedFilterConfig);

    }

    @After
    public void afterTestsCallMethodDestroy() {
        languageFilter.destroy();
    }


    @Test
    public void testFilterSetPriorityLanguage_US_IfUserFirstPriorityLocale_US() throws ServletException, IOException {
        Enumeration browserLocales;
        Vector locales = new Vector();
        locales.add(Locale.US);
        locales.add(Locale.FRENCH);
        locales.add(Locale.GERMANY);
        browserLocales = locales.elements();
        Mockito.when(mockedRequest.getLocales()).thenReturn(browserLocales);
        Locale localeBefore = (Locale) mockedRequest.getAttribute("lang");
        Locale localeExpected = Locale.US;
        languageFilter.doFilter(mockedRequest, mockedResponse, mockFilterChain);
        Locale localeAfter = (Locale) mockedRequest.getAttribute("lang");
        assertEquals(null, localeBefore);
        assertEquals(localeExpected, localeAfter);
    }

    @Test
    public void testFilterSetPriorityLanguage_GE_IfUserFirstLocaleDoNotSupportedButSupportedSecond_GE() throws ServletException, IOException {
        Enumeration browserLocales;
        Vector locales = new Vector();
        locales.add(Locale.FRENCH);
        locales.add(Locale.GERMANY);
        locales.add(Locale.US);
        browserLocales = locales.elements();
        Mockito.when(mockedRequest.getLocales()).thenReturn(browserLocales);
        Locale localeBefore = (Locale) mockedRequest.getAttribute("lang");
        Locale localeExpected = Locale.GERMANY;
        languageFilter.doFilter(mockedRequest, mockedResponse, mockFilterChain);
        Locale localeAfter = (Locale) mockedRequest.getAttribute("lang");
        assertEquals(null, localeBefore);
        assertEquals(localeExpected, localeAfter);
    }

    @Test
    public void testFilterSetPriorityLanguage_US_IfUserFirstAndSecondLocaleDoNotSupportedButSupportedThird_US() throws ServletException, IOException {
        Enumeration browserLocales;
        Vector locales = new Vector();
        locales.add(Locale.FRENCH);
        locales.add(Locale.ITALIAN);
        locales.add(Locale.US);
        browserLocales = locales.elements();
        Mockito.when(mockedRequest.getLocales()).thenReturn(browserLocales);
        Locale localeBefore = (Locale) mockedRequest.getAttribute("lang");
        Locale localeExpected = Locale.US;
        languageFilter.doFilter(mockedRequest, mockedResponse, mockFilterChain);
        Locale localeAfter = (Locale) mockedRequest.getAttribute("lang");
        assertEquals(null, localeBefore);
        assertEquals(localeExpected, localeAfter);
    }

    @Test
    public void testFilterSetPriorityLanguageDefault_RU_IfAllUserLocalesDoNotSupported() throws ServletException, IOException {
        Enumeration browserLocales;
        Vector locales = new Vector();
        locales.add(Locale.FRENCH);
        locales.add(Locale.ITALIAN);
        locales.add(Locale.CHINESE);
        browserLocales = locales.elements();
        Mockito.when(mockedRequest.getLocales()).thenReturn(browserLocales);
        Locale localeBefore = (Locale) mockedRequest.getAttribute("lang");
        Locale localeExpected = new Locale("ru", "RU");
        languageFilter.doFilter(mockedRequest, mockedResponse, mockFilterChain);
        Locale localeAfter = (Locale) mockedRequest.getAttribute("lang");
        assertEquals(null, localeBefore);
        assertEquals(localeExpected, localeAfter);
    }


}