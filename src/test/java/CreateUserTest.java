import entity.User;
import org.junit.Before;
import org.junit.Test;
import repository.DaoUser;
import repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CreateUserTest {
    DaoUser userRepository = new UserRepository();
    User user1 = new User(1, "userName1", "userSurname1", "userEmail1@email.com", "user1password", "///", true);
    User user2 = new User(2, "userName2", "userSurname2", "userEmail2@email.com", "user2password", "///", false);

    @Before
    public void cleanUserList() {
        userRepository.deleteUserById(1);
        userRepository.deleteUserById(2);
    }

    @Test
    public void userGetAllTestPassedIfReturnedListOfAllUsersContainsAllAddedUsers() {
        userRepository.addUser(new User(1, "userName1", "userSurname1", "userEmail1@email.com", "user1password", "///", true));
        userRepository.addUser(new User(2, "userName2", "userSurname2", "userEmail2@email.com", "user2password", "///", false));
        List<User> list = new ArrayList<User>();
        list.add(user1);
        list.add(user2);
        assertEquals(userRepository.getAll().toString(), list.toString());
    }

    @Test
    public void addUserTestPassedIfReturnTrueAfterAddedUserAndUserListContainsNewUsers() {
        List<User> list = new ArrayList<User>();
        list.add(user1);
        list.add(user2);
        assertTrue(userRepository.addUser(user1));
        assertTrue(userRepository.addUser(user2));
        assertEquals(userRepository.getAll().toString(), list.toString());
    }

    @Test
    public void deleteUserByIdTestPassedIfReturnTrueAfterDeleteUserAndListDoesNotContainsThisUsers() {
        userRepository.addUser(user1);
        userRepository.addUser(user2);
        assertTrue(userRepository.deleteUserById(1));
        assertTrue(userRepository.deleteUserById(2));

        assertNull(userRepository.getUserById(user1.getId()));
        assertNull(userRepository.getUserById(user2.getId()));
    }

    @Test
    public void getUserByIdTestPassedIfReturnTrueOnExpectedUserAndFalseOnWrongUser() {
        userRepository.addUser(user1);
        assertEquals(user1, userRepository.getUserById(1));
        assertFalse(user2 == userRepository.getUserById(1));
    }
}